package com.enset.payments.refundexecutor.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;
import com.enset.payments.refundexecutor.service.RefundService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;


@RestController
@RequestMapping("/api/v1/refund")
public class RefundController {


    private final InboundLogger inboundLogger;
    private final OutboundLogger outboundLogger;
    private final RefundService refundService;

    public RefundController(InboundLogger inboundLogger, OutboundLogger outboundLogger, RefundService refundService) {
        this.inboundLogger = inboundLogger;
        this.outboundLogger = outboundLogger;
        this.refundService = refundService;
    }

    @PostMapping
    public ResponseEntity<Void> refund(
            @Valid @RequestBody final PaymentReferenceRequest request,
            @NotBlank @RequestHeader("X-Tenant") String tenant) {
        inboundLogger.info("Refund", request);

        refundService.refundByPaymentRef(request.paymentReference(), tenant);

        outboundLogger.info("Refund", null);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
