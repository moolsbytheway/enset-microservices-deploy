package com.enset.payments.refundexecutor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = {"com.enset.payments"})
public class RefundExecutorApplication {

    public static void main(String[] args) {
        SpringApplication.run(RefundExecutorApplication.class, args);
    }

}
