package com.enset.payments.refundexecutor.service;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;
import com.enset.payments.refundexecutor.repository.TransactionDocument;
import com.enset.payments.refundexecutor.repository.TransactionRepository;
import com.enset.payments.refundexecutor.repository.TransactionStatus;
import com.enset.payments.refundexecutor.tenant.TenantValidator;

@Component
public class RefundService {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    private final TransactionRepository transactionRepository;
    private final TenantValidator tenantValidator;

    public RefundService(TransactionRepository transactionRepository, TenantValidator tenantValidator) {
        this.transactionRepository = transactionRepository;
        this.tenantValidator = tenantValidator;
    }

    public void refundByPaymentRef(String paymentReference, String tenant) {
        Optional<TransactionDocument> entity = transactionRepository.findTransactionDocumentByPaymentRef(paymentReference);

        if (entity.isEmpty()) {
            throw new BusinessException(ErrorCodes.TRANSACTION_MISSING);
        }

        tenantValidator.validate(tenant, entity.get().getTenant());

        if (TransactionStatus.REFUNDED.equals(entity.get().getStatus())) {
            throw new BusinessException(ErrorCodes.TRANSACTION_REFUNDED);
        }

        if (!TransactionStatus.COMPLETED.equals(entity.get().getStatus())) {
            throw new BusinessException(ErrorCodes.TRANSACTION_NOT_COMPLETE);
        }

        transactionRepository.updateTransactionStatus(paymentReference, TransactionStatus.REFUNDED, Instant.now());

        LOG.info("Refunded transaction with reference: {}", paymentReference);
    }

    @Scheduled(fixedRateString = "${refund.fixed-rate-in-seconds}", initialDelay = 10 * 1000) // 10 seconds delay
    @Transactional
    public void refund() {
        LOG.info("Refund executor - Searching refundable transactions");
        Instant tenMinutesAgo = Instant.now().minusSeconds(10 * 60);
        Set<Long> refundableTransactions = transactionRepository.findAllByStatus(TransactionStatus.CAPTURE_SUCCESS, tenMinutesAgo);
        if (refundableTransactions.isEmpty()) {
            LOG.info("Refund executor - No refundable transactions found");
            return;
        }

        doRefund(refundableTransactions);

    }

    private void doRefund(Set<Long> refundableTransactions) {
        transactionRepository.updateTransactionStatusByIds(TransactionStatus.REFUNDED, Instant.now(), refundableTransactions);
        LOG.info("Refunded transactions {}", refundableTransactions);
    }
}
