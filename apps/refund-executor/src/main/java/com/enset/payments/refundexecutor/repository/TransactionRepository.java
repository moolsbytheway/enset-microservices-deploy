package com.enset.payments.refundexecutor.repository;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import jakarta.transaction.Transactional;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionDocument, Long> {

    @Query("update TransactionDocument t set t.status = ?1, t.lastUpdatedAt = ?2 where t.id in (?3)")
    @Modifying
    @Transactional
    void updateTransactionStatusByIds(TransactionStatus status, Instant lastUpdate, Set<Long> ids);


    @Query("update TransactionDocument t set t.status = ?2, t.lastUpdatedAt = ?3 where t.paymentRef = ?1")
    @Modifying
    @Transactional
    void updateTransactionStatus(String paymentRef, TransactionStatus status, Instant lastUpdatedAt);

    @Query("SELECT t.id FROM TransactionDocument t WHERE t.status = ?1 AND t.lastUpdatedAt < ?2")
    Set<Long> findAllByStatus(TransactionStatus status, Instant timeLimit);


    @Query("SELECT t.id FROM TransactionDocument t WHERE t.paymentRef = ?1")
    Set<Long> findByPaymentReference(String paymentRef);

    Optional<TransactionDocument> findTransactionDocumentByPaymentRef(String paymentRef);


}
