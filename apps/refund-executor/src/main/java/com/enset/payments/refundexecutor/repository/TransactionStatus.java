package com.enset.payments.refundexecutor.repository;

public enum TransactionStatus {
    CAPTURE_SUCCESS,
    CAPTURE_FAILURE,
    COMPLETED,
    REFUNDED
}
