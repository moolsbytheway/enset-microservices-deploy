package com.enset.payments.refundexecutor.repository;

public enum PaymentMethodType {
    CREDIT_DEBIT_CARD,
    APPLE_PAY,
    PAYPAL
}
