package com.enset.payments.common.logger;

public enum ServiceKeys {
    PROCESSOR,
    REDIS,
    TOKENIZATION
}
