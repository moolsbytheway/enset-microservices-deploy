package com.enset.payments.common.exception;

public enum ErrorCodes {

    // bff
    SESSION_EXPIRED("Session expired or missing"),
    // tokenization
    OTT_EXPIRED("One time token expired"),

    // API KEY
    INVALID_API_KEY("Provided API key is invalid for current operation"),
    INVALID_API_KEY_SCOPE("API key scope is invalid for current operation"),

    // processor
    TRANSACTION_MISSING("Transaction doesn't exist anymore"),
    PAYMENT_FAILED_BEFORE_COMPLETE("Can't complete failed payment"),
    TRANSACTION_COMPLETE_OR_REFUNDED("Transaction already complete or refunded"),
    TRANSACTION_REFUNDED("Transaction already refunded"),
    TRANSACTION_NOT_COMPLETE("Can't refund uncomplete transaction"),

    // Tenant
    INVALID_TENANT("Tenant not authorized to access this transaction"),

    CLIENT_ERROR("Client error"),
    GENERIC_SERVER_ERROR("Generic server error, check logs for more details");

    private final String message;

    ErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
