package com.enset.payments.common.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.enset.payments.common.config.AwsSecretsManagerConfiguration;

@Configuration
@ConditionalOnProperty(prefix = "common.awssecrets",
        name = "enabled",
        havingValue = "true")
@Import(AwsSecretsManagerConfiguration.class)
public class AwsAutoConfiguration {

}
