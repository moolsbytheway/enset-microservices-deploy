package com.enset.payments.common.config;

import static com.enset.payments.common.config.JacksonConfiguration.STATIC_OBJECT_MAPPER;

import java.io.IOException;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorMessage;

import feign.Response;
import feign.codec.ErrorDecoder;

public class FeignErrorDecoder implements ErrorDecoder {
    private final Default defaultDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() >= 400 && response.status() < 600) {
            try {
                var errorMessage = STATIC_OBJECT_MAPPER.readValue(response.body().asInputStream(), ErrorMessage.class);
                throw new BusinessException(errorMessage.getErrorCode());
            } catch (IOException e) {
                //throw new RuntimeException(e);
            }
        }
        return defaultDecoder.decode(methodKey, response);
    }
}
