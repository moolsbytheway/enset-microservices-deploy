package com.enset.payments.common.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.enset.payments.common.config.JacksonConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ConditionalOnClass(ObjectMapper.class)
@Import(JacksonConfiguration.class)
public class JacksonConfigAutoConfiguration {

}
