package com.enset.payments.common.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class LuhnAlgorithmConstraintValidator
        implements ConstraintValidator<LuhnAlgorithmValid, String> {

    @Override
    public void initialize(final LuhnAlgorithmValid constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(
            final String cardNumber,
            final ConstraintValidatorContext constraintValidatorContext) {

        if (cardNumber == null || cardNumber.isEmpty()) return false;

        int numberOfDigits = cardNumber.length();
        int sum = 0;
        boolean isSecond = false;
        for (int i = numberOfDigits - 1; i >= 0; i--) {
            int d = cardNumber.charAt(i) - '0';
            if (isSecond) d = d * 2;
            // we add two digits to handle cases that make two digits after doubling
            sum += d / 10;
            sum += d % 10;

            isSecond = !isSecond;
        }
        return (sum % 10 == 0);
    }
}
