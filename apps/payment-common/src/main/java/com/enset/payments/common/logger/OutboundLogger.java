package com.enset.payments.common.logger;

import static com.enset.payments.common.config.JacksonConfiguration.STATIC_OBJECT_MAPPER;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

@Component
public class OutboundLogger {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    public void info(ServiceKeys serviceKey, String method, Object params) {
        try {
            LOG.info("Client Outbound -> {} - {} [{}]", serviceKey, method, STATIC_OBJECT_MAPPER.writeValueAsString(params));
        } catch (JsonProcessingException e) {
            LOG.info("Client Outbound -> {} - {} [error - couldn't serialize params]", serviceKey, method);
        }
    }

    public void info(String method, Object params) {
        try {
            LOG.info("Outbound -> {} [{}]", method, STATIC_OBJECT_MAPPER.writeValueAsString(params));
        } catch (JsonProcessingException e) {
            LOG.info("Outbound -> {} [error - couldn't serialize params]", method);
        }
    }
}
