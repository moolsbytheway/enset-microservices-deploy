package com.enset.payments.common.config;

import static com.enset.payments.common.config.JacksonConfiguration.STATIC_OBJECT_MAPPER;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;

@AutoConfiguration
@ConditionalOnProperty(prefix = "common.awssecrets",
        name = "enabled",
        havingValue = "true")
public class AwsSecretsManagerConfiguration {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    @Value("${aws.secret.name}")
    private String secretName;

    @Value("${aws.region}")
    private String awsRegion;

    @Bean
    @Order(1)
    public AwsSecrets awsSecrets() throws IOException {
        LOG.info("Fetching secrets from aws secrets manager...");
        Region region = Region.of(awsRegion);
        SecretsManagerClient client = SecretsManagerClient.builder()
                .region(region)
                .build();

        GetSecretValueRequest getSecretValueRequest = GetSecretValueRequest.builder()
                .secretId(secretName)
                .build();

        GetSecretValueResponse getSecretValueResponse = client.getSecretValue(getSecretValueRequest);

        String secretString = getSecretValueResponse.secretString();
        LOG.info("Fetching secrets from aws secrets manager - DONE");

        LOG.info("Deserializing secrets...");
        AwsSecrets awsSecrets = STATIC_OBJECT_MAPPER.readValue(secretString, AwsSecrets.class);
        Map<String, String> secretMap = STATIC_OBJECT_MAPPER.readValue(secretString, Map.class);

        secretMap.forEach((k, v) -> {
            if (k.startsWith("apikey")) {
                awsSecrets.addApiKey(k, v);
            }
        });
        LOG.info("Deserializing secrets - DONE");
        return awsSecrets;
    }
}
