package com.enset.payments.common.config;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

import feign.Logger;
import feign.Request;
import feign.Retryer;
import feign.codec.ErrorDecoder;

@AutoConfiguration
public class FeignConfiguration {
    @Value("${clients.http.connect-timeout-seconds}")
    private int connectTimeout;

    @Value("${clients.http.read-timeout-seconds}")
    private int readTimout;

    @Value("${clients.http.retry-period-seconds}")
    private int retryPeriod;

    @Value("${clients.http.retry-attempts}")
    private int retryAttempts;

    @Bean
    public Request.Options requestOptions() {
        return new Request.Options(Duration.ofSeconds(connectTimeout), Duration.ofSeconds(readTimout), true);
    }

    @Bean
    public Retryer retryer() {
        return new Retryer.Default(SECONDS.toMillis(retryPeriod), SECONDS.toMillis(retryPeriod), retryAttempts);
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.BASIC;
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new FeignErrorDecoder();
    }
}
