package com.enset.payments.common.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Constraint(validatedBy = LuhnAlgorithmConstraintValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LuhnAlgorithmValid {

    String message() default "Credit card number must be valid per Luhn's algorithm.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
