package com.enset.payments.common.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AwsSecrets {
    @JsonProperty("payment-processor.sql-database.username")
    private String mariaDbUsername;
    @JsonProperty("payment-processor.sql-database.password")
    private String mariaDbPassword;
    @JsonProperty("payment-processor.sql-database.database-name")
    private String mariaDbDatabaseName;
    @JsonProperty("bff.redis-database.password")
    private String redisDbPassword;

    private Map<String, String> apiKeys = new HashMap<>();

    public AwsSecrets() {
    }

    public AwsSecrets(String mariaDbUsername, String mariaDbPassword, String mariaDbDatabaseName, String redisDbPassword) {
        this.mariaDbUsername = mariaDbUsername;
        this.mariaDbPassword = mariaDbPassword;
        this.mariaDbDatabaseName = mariaDbDatabaseName;
        this.redisDbPassword = redisDbPassword;
    }

    public String getMariaDbUsername() {
        return mariaDbUsername;
    }

    public void setMariaDbUsername(String mariaDbUsername) {
        this.mariaDbUsername = mariaDbUsername;
    }

    public String getMariaDbPassword() {
        return mariaDbPassword;
    }

    public void setMariaDbPassword(String mariaDbPassword) {
        this.mariaDbPassword = mariaDbPassword;
    }

    public String getMariaDbDatabaseName() {
        return mariaDbDatabaseName;
    }

    public void setMariaDbDatabaseName(String mariaDbDatabaseName) {
        this.mariaDbDatabaseName = mariaDbDatabaseName;
    }

    public String getRedisDbPassword() {
        return redisDbPassword;
    }

    public void setRedisDbPassword(String redisDbPassword) {
        this.redisDbPassword = redisDbPassword;
    }

    public Optional<String> getApiKey(String key) {
        if (apiKeys == null) return Optional.empty();
        return Optional.ofNullable(apiKeys.get(key));
    }

    public void addApiKey(String key, String value) {
        if (this.apiKeys == null) {
            this.apiKeys = new HashMap<>();
        }
        this.apiKeys.put(key, value);
    }
}
