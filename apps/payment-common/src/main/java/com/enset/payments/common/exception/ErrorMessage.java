package com.enset.payments.common.exception;

import java.time.Instant;


public class ErrorMessage {
    private String timestamp;
    private int status;
    private ErrorCodes errorCode;
    private String message;

    public ErrorMessage() {
    }

    public ErrorMessage(int status, String message) {
        this.status = status;
        this.errorCode = ErrorCodes.CLIENT_ERROR;
        this.message = message;
        this.timestamp = Instant.now().toString();
    }

    public ErrorMessage(int status, ErrorCodes errorCode) {
        this.status = status;
        this.errorCode = errorCode;
        this.message = errorCode.getMessage();
        this.timestamp = Instant.now().toString();
    }

    public String getTimestamp() {
        return timestamp;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }


    public ErrorCodes getErrorCode() {
        return errorCode;
    }
}
