package com.enset.payments.common.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.enset.payments.common.config.FeignConfiguration;

@Configuration
@ConditionalOnProperty(prefix = "common.feign",
        name = "enabled",
        havingValue = "true")
@Import(FeignConfiguration.class)
public class FeignAutoConfiguration {

}
