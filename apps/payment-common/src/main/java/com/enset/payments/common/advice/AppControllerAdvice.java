package com.enset.payments.common.advice;

import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorMessage;

@ControllerAdvice
public class AppControllerAdvice {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    @ExceptionHandler(BusinessException.class)
    public final ResponseEntity<ErrorMessage> handleBusinessException(BusinessException ex, WebRequest request) {
        LOG.error(ex.getErrorCode().getMessage(), ex);
        return new ResponseEntity<>(new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getErrorCode()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> handleBadRequestException(MethodArgumentNotValidException ex, WebRequest request) {
        String message = ex.getBindingResult()
                .getAllErrors()
                .stream()
                .filter(it -> it instanceof FieldError)
                .map((it) -> {
                    var fieldError = (FieldError) it;
                    return String.format("field [%s] %s", fieldError.getField(), fieldError.getDefaultMessage());
                })
                .collect(Collectors.joining(", "));
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), message);
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ResponseEntity<ErrorMessage> handleBadRequestException(MissingRequestHeaderException ex, WebRequest request) {
        String header = ex.getHeaderName();
        String message = String.format("Required request header '%s' is not present", header);
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), message);
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }
}
