package com.enset.payments.common.exception;

public class BusinessException extends RuntimeException {
    private final ErrorCodes errorCode;

    public BusinessException(ErrorCodes errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public ErrorCodes getErrorCode() {
        return errorCode;
    }
}
