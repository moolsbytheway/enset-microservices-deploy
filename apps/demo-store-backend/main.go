package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Customer struct {
	CustomerID string `json:"customer_id"`
	Name       string `json:"name"`
	Email      string `json:"email"`
	Address    string `json:"address"`
	Phone      string `json:"phone"`
}

type Order struct {
	OrderID     string  `json:"order_id"`
	Description string  `json:"description"`
	Amount      float64 `json:"amount"`
	Quantity    int     `json:"quantity"`
	Image       string  `json:"image"`
}

type CreateSessionRequest struct {
	PaymentMethodType string   `json:"payment_method_type"`
	CallbackURL       string   `json:"callback_url"`
	CheckoutURL       string   `json:"checkout_url"`
	Customer          Customer `json:"customer"`
	Order             Order    `json:"order"`
}

var apiKey = "YXBpa2V5LnNlc3Npb24uZGVtby1zdG9yZS5jYW1lbGNvZGVzLm5ldDpucWgzZFQ4M3IwT0xaMVVHZ1FsSEdGbldFTGJNaVY4Mw=="

// allowCORS is a middleware to allow CORS requests
func allowCORS(handler http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Allow CORS by setting headers zz
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

		// Handle preflight OPTIONS request
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		// Call the next handler
		handler.ServeHTTP(w, r)
	}
}

func main() {
	http.HandleFunc("/buy", createSessionHandler)
	http.HandleFunc("/payment_callback", paymentCallbackHandler)
	fmt.Println("Server listening...")
	http.ListenAndServe(":9090", allowCORS(http.DefaultServeMux))
}

func paymentCallbackHandler(w http.ResponseWriter, r *http.Request) {
	// Parse query parameters
	paymentReference := r.URL.Query().Get("payment_reference")
	if paymentReference == "" {
		http.Error(w, "payment_reference query parameter is required", http.StatusBadRequest)
		return
	}

	// Call paymentservice.neapi/v1/payments/complete
	completeURL := "https://payments-backend.camelcodes.net/api/v1/payments/complete"
	requestBody := map[string]string{"payment_reference": paymentReference}
	completeResp, err := callPaymentService(completeURL, requestBody)
	if err != nil || completeResp.StatusCode != http.StatusNoContent {
		fmt.Println("Error", err)
		redirectTo(w, r, "https://demo-store.camelcodes.net/payment_error.html?error_type=complete&payment_reference="+paymentReference)
		return
	}

	// Call paymentservice.net/api/v1/payments/verify-payment-result
	verifyURL := "https://payments-backend.camelcodes.net/api/v1/payments/verify-payment-result"
	verifyResp, err := callPaymentService(verifyURL, requestBody)
	if err != nil || verifyResp.StatusCode != http.StatusOK {
		fmt.Println("Error", err)
		redirectTo(w, r, "https://demo-store.camelcodes.net/payment_error.html?error_type=payment-result&payment_reference="+paymentReference)
		return
	}

	// Parse the response body from verify payment result
	var responseBody struct {
		Status string `json:"status"`
	}
	if err := json.NewDecoder(verifyResp.Body).Decode(&responseBody); err != nil {
		http.Error(w, "Failed to decode verify payment result response", http.StatusInternalServerError)
		return
	}

	// Check if status is COMPLETED
	if responseBody.Status != "COMPLETED" {
		redirectTo(w, r, "https://demo-store.camelcodes.net/payment_error.html?error_type=payment-result-status&payment_reference="+paymentReference)
		return
	}

	// Redirect to payment success page
	redirectTo(w, r, "https://demo-store.camelcodes.net/payment_success.html")
}

// Function to make HTTP requests to payment services
func callPaymentService(url string, requestBody map[string]string) (*http.Response, error) {
	requestBodyBytes, _ := json.Marshal(requestBody)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBodyBytes))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-API-KEY", apiKey)

	client := &http.Client{}
	return client.Do(req)
}

// Function to perform a redirect
func redirectTo(w http.ResponseWriter, r *http.Request, location string) {
	http.Redirect(w, r, location, http.StatusSeeOther)
}

func createSessionHandler(w http.ResponseWriter, r *http.Request) {
	// Parse request body
	var reqBody struct {
		Name     string `json:"name"`
		Email    string `json:"email"`
		Phone    string `json:"phone"`
		Address  string `json:"address"`
		Quantity int    `json:"quantity"`
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		http.Error(w, "Failed to parse request body", http.StatusBadRequest)
		return
	}

	// Prepare payload for payments-backend.camelcodes.net
	createSessionReq := CreateSessionRequest{
		PaymentMethodType: "CREDIT_DEBIT_CARD",
		CallbackURL:       "https://demo-store-backend.camelcodes.net/payment_callback",
		CheckoutURL:       "https://demo-store.camelcodes.net/checkout.html?order=172672",
		Customer: Customer{
			CustomerID: "192737382",
			Name:       reqBody.Name,
			Email:      reqBody.Email,
			Address:    reqBody.Address,
			Phone:      reqBody.Phone,
		},
		Order: Order{
			OrderID:     "172672",
			Description: "Sunny Hill Festival Event",
			Amount:      90,
			Quantity:    reqBody.Quantity,
			Image:       "https://demo-store.camelcodes.net/assets/images/ticket-details.jpg",
		},
	}

	// Marshal payload to JSON
	payload, err := json.Marshal(createSessionReq)
	if err != nil {
		http.Error(w, "Failed to marshal JSON", http.StatusInternalServerError)
		return
	}

	// Send POST request to payments-backend.camelcodes.net
	req, err := http.NewRequest("POST", "https://payments-backend.camelcodes.net/api/v1/payments/sessions",
		bytes.NewBuffer(payload))

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-API-KEY", apiKey)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		http.Error(w, "Failed to send request to payments-backend.camelcodes.net", http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	// Read response body
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, "Failed to read response body", http.StatusInternalServerError)
		return
	}

	// Set response content type
	w.Header().Set("Content-Type", "application/json")
	// Set response status code (optional)
	w.WriteHeader(resp.StatusCode)
	// Write response body
	w.Write(respBody)
}
