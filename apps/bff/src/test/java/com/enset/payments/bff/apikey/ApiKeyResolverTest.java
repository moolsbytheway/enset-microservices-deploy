package com.enset.payments.bff.apikey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Base64;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.enset.payments.common.config.AwsSecrets;
import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;

@ExtendWith(MockitoExtension.class)
public class ApiKeyResolverTest {

    @Mock
    private AwsSecrets awsSecrets;

    @InjectMocks
    private ApiKeyResolver apiKeyResolver;

    @BeforeEach
    public void setUp() {
        apiKeyResolver = new ApiKeyResolver(awsSecrets);
    }

    @Test
    public void testValidApiKeyWithCorrectScope() {
        ApiKeyScope apiKeyScope = ApiKeyScope.REFUND;
        String apiKeyDomain = "apikey.refund.demo-store.camelcodes.net";
        String apiKeyValue = "z3OddqWT7f6FKbdQnY4qb4fmyifu9npL";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        when(awsSecrets.getApiKey(apiKeyDomain)).thenReturn(Optional.of(apiKeyValue));

        apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64);
    }

    @Test
    public void testInvalidBase64Encoding() {
        String invalidBase64 = "invalidBase64String";

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(ApiKeyScope.REFUND, invalidBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY, exception.getErrorCode());
    }

    @Test
    public void testApiKeyWithIncorrectScope() {
        ApiKeyScope apiKeyScope = ApiKeyScope.REFUND;
        String apiKeyDomain = "apikey.session.demo-store.camelcodes.net"; // Incorrect scope
        String apiKeyValue = "z3OddqWT7f6FKbdQnY4qb4fmyifu9npL";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY_SCOPE, exception.getErrorCode());
    }

    @Test
    public void testApiKeyNotFoundInSecrets() {
        ApiKeyScope apiKeyScope = ApiKeyScope.REFUND;
        String apiKeyDomain = "apikey.refund.demo-store.camelcodes.net";
        String apiKeyValue = "z3OddqWT7f6FKbdQnY4qb4fmyifu9npL";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY, exception.getErrorCode());
    }

    @Test
    public void testApiKeyValueMismatch() {
        ApiKeyScope apiKeyScope = ApiKeyScope.REFUND;
        String apiKeyDomain = "apikey.refund.demo-store.camelcodes.net";
        String apiKeyValue = "z3OddqWT7f6FKbdQnY4qb4fmyifu9npL";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY, exception.getErrorCode());

    }

    @Test
    public void testValidApiKeyWithSessionScope() {
        ApiKeyScope apiKeyScope = ApiKeyScope.SESSION;
        String apiKeyDomain = "apikey.session.demo-store.camelcodes.net";
        String apiKeyValue = "s3cureS3ssionKey";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY, exception.getErrorCode());

    }

    @Test
    public void testApiKeyWithIncorrectScopeForSession() {
        ApiKeyScope apiKeyScope = ApiKeyScope.SESSION;
        String apiKeyDomain = "apikey.refund.demo-store.camelcodes.net"; // Incorrect scope
        String apiKeyValue = "s3cureS3ssionKey";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY_SCOPE, exception.getErrorCode());

    }

    @Test
    public void testApiKeyNotFoundInSecretsForSession() {
        ApiKeyScope apiKeyScope = ApiKeyScope.SESSION;
        String apiKeyDomain = "apikey.session.demo-store.camelcodes.net";
        String apiKeyValue = "s3cureS3ssionKey";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY, exception.getErrorCode());
    }

    @Test
    public void testApiKeyValueMismatchForSession() {
        ApiKeyScope apiKeyScope = ApiKeyScope.SESSION;
        String apiKeyDomain = "apikey.session.demo-store.camelcodes.net";
        String apiKeyValue = "s3cureS3ssionKey";
        String apiKey = apiKeyDomain + ":" + apiKeyValue;
        String apiKeyBase64 = Base64.getEncoder().encodeToString(apiKey.getBytes());

        BusinessException exception = assertThrows(BusinessException.class, () ->
                apiKeyResolver.assertApiKeyIsValidForScope(apiKeyScope, apiKeyBase64));

        assertEquals(ErrorCodes.INVALID_API_KEY, exception.getErrorCode());
    }

}
