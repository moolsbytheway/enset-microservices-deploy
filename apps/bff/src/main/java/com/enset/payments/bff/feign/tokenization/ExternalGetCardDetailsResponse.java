package com.enset.payments.bff.feign.tokenization;

public record ExternalGetCardDetailsResponse(String pan, Integer expiryYear, Integer expiryMonth,
                                             String cardholderName) {
}
