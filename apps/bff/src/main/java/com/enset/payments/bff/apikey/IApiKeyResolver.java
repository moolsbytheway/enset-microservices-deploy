package com.enset.payments.bff.apikey;

public interface IApiKeyResolver {
    void assertApiKeyIsValidForScope(ApiKeyScope apiKeyScope, String apiKeyBase64String);
}
