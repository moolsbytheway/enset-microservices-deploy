package com.enset.payments.bff.domain.response;

import java.time.Instant;

public record AuthorizeAndCaptureResponse(
        String status,
        String paymentReference,
        Instant dateTime,
        AuthorizeAndCapturePaymentMethodResponse paymentMethod) {
}
