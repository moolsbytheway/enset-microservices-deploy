package com.enset.payments.bff.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enset.payments.bff.apikey.ApiKeyScope;
import com.enset.payments.bff.apikey.IApiKeyResolver;
import com.enset.payments.bff.apikey.TenantExtractor;
import com.enset.payments.bff.domain.request.CreateSessionRequest;
import com.enset.payments.bff.domain.response.CreateSessionResponse;
import com.enset.payments.bff.domain.response.GetSessionResponse;
import com.enset.payments.bff.service.SessionService;
import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;


@RestController
@RequestMapping(PathsDefinitions.PAYMENTS_V1)
public class SessionController {

    private final InboundLogger inboundLogger;
    private final OutboundLogger outboundLogger;

    private final SessionService sessionService;
    private final IApiKeyResolver apiKeyResolver;

    public SessionController(
            InboundLogger inboundLogger, OutboundLogger outboundLogger, final SessionService sessionService, IApiKeyResolver apiKeyResolver) {
        this.inboundLogger = inboundLogger;
        this.outboundLogger = outboundLogger;
        this.sessionService = sessionService;
        this.apiKeyResolver = apiKeyResolver;
    }

    @PostMapping(path = PathsDefinitions.SESSIONS)
    public ResponseEntity<CreateSessionResponse> createSession(
            @Valid @RequestBody final CreateSessionRequest request,
            @NotBlank @RequestHeader("X-API-KEY") String apiKey) {

        inboundLogger.info("Create session", request);

        apiKeyResolver.assertApiKeyIsValidForScope(ApiKeyScope.SESSION, apiKey);

        String tenant = TenantExtractor.extractTenantFromApiKey(apiKey);

        final CreateSessionResponse response = sessionService.createSession(request, tenant);

        outboundLogger.info("Create session", response);

        return ResponseEntity.ok(response);
    }

    @GetMapping(path = PathsDefinitions.SESSIONS)
    public ResponseEntity<GetSessionResponse> getSession(
            @NotBlank @RequestHeader("Authorization") String sessionToken) {

        inboundLogger.info("Get session", sessionToken);

        final GetSessionResponse response = sessionService.getSession(sessionToken);
        outboundLogger.info("Get session", response);

        return ResponseEntity.ok(response);
    }
}
