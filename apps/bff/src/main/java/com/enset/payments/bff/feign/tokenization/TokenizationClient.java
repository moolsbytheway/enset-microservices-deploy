package com.enset.payments.bff.feign.tokenization;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import com.enset.payments.bff.domain.request.TokenizationRequest;
import com.enset.payments.bff.domain.response.TokenizationResponse;
import com.enset.payments.common.config.FeignConfiguration;


@FeignClient(name = "tokenizationClient",
        url = "${clients.tokenization.base-url}",
        configuration = FeignConfiguration.class)
public interface TokenizationClient {
    @PostMapping("/vault")
    ResponseEntity<TokenizationResponse> vault(TokenizationRequest tokenizationRequest);
}
