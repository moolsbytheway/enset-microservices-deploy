package com.enset.payments.bff.domain.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

public record CreateSessionRequest(
        @NotNull @Valid Customer customer,
        @NotNull @Valid Order order,
        @NotBlank String checkoutUrl,
        @NotBlank String callbackUrl,
        @NotNull @Pattern(regexp = "(CREDIT_DEBIT_CARD|APPLE_PAY|PAYPAL)") String paymentMethodType
) {
}
