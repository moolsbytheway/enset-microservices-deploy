package com.enset.payments.bff.apikey;

public enum ApiKeyScope {
    SESSION,
    REFUND
}
