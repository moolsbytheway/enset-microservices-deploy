package com.enset.payments.bff.feign.processor.request;

public record ExternalAuthorizeAndCapturePaymentMethod(
        String type,
        Double amount,
        String oneTimeToken) {
}
