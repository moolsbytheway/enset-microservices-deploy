package com.enset.payments.bff.feign.tokenization;

public record ExternalGetCardDetailsRequest(String ott) {
}
