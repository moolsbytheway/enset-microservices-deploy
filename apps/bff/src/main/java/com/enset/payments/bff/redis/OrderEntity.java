package com.enset.payments.bff.redis;

public class OrderEntity {
    private String orderId;
    private String description;
    private Double amount;
    private Integer quantity;
    private String image;

    public OrderEntity() {
    }

    public OrderEntity(String orderId, String description, Double amount, Integer quantity, String image) {
        this.orderId = orderId;
        this.description = description;
        this.amount = amount;
        this.quantity = quantity;
        this.image = image;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
