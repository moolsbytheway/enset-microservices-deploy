package com.enset.payments.bff.feign.processor.request;

public record ExternalAuthorizeAndCaptureRequest(
        String customerId,
        ExternalAuthorizeAndCapturePaymentMethod paymentMethod,
        String orderId) {
}
