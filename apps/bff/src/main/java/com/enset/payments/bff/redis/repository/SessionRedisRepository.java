package com.enset.payments.bff.redis.repository;


import static com.enset.payments.common.logger.ServiceKeys.REDIS;

import java.time.Duration;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import com.enset.payments.bff.redis.SessionEntity;
import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;

@Repository
public class SessionRedisRepository implements IRedisRepository<SessionEntity, String> {

    @Value("${session.expiration-time-in-minutes}")
    private Integer sessionExpirationTimeInMinutes;

    private final OutboundLogger outboundLogger;
    private final InboundLogger inboundLogger;
    private final StringRedisTemplate redisTemplate;

    public SessionRedisRepository(OutboundLogger outboundLogger, InboundLogger inboundLogger, StringRedisTemplate redisTemplate) {
        this.outboundLogger = outboundLogger;
        this.inboundLogger = inboundLogger;
        this.redisTemplate = redisTemplate;
    }


    public Optional<SessionEntity> findById(String sessionId) {
        String redisKey = getKey(sessionId);

        if (!Boolean.TRUE.equals(redisTemplate.hasKey(redisKey))) return Optional.empty();

        outboundLogger.info(REDIS, "GET Session", redisKey);
        String redisValue = redisTemplate.opsForValue().get(redisKey);
        inboundLogger.info(REDIS, "GET Session", redisValue);
        return Optional.ofNullable(readValue(redisValue, SessionEntity.class));
    }

    public void deleteById(String sessionId) {
        String redisKey = getKey(sessionId);

        if (Boolean.TRUE.equals(redisTemplate.hasKey(redisKey))) {
            outboundLogger.info(REDIS, "DELETE Session", redisKey);
            redisTemplate.delete(redisKey);
        }

    }

    public void save(SessionEntity entity) {
        String redisKey = getKey(entity.getSessionId());

        outboundLogger.info(REDIS, "CREATE Session", redisKey);
        redisTemplate.opsForValue().set(redisKey, writeValue(entity), Duration.ofMinutes(sessionExpirationTimeInMinutes));
    }

    public String getKey(String id) {
        return "session." + id;
    }

}
