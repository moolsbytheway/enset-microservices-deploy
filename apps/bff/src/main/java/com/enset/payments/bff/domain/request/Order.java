package com.enset.payments.bff.domain.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;

public record Order(
        @NotBlank @Size(min = 6, max = 6) String orderId,
        @NotBlank @Size(min = 3, max = 64) String description,
        @NotNull @Positive Double amount,
        @NotNull @Positive Integer quantity,
        @Size(max = 128) String image) {
}
