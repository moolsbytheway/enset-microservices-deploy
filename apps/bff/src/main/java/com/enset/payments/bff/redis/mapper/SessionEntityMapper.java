package com.enset.payments.bff.redis.mapper;

import org.springframework.stereotype.Component;

import com.enset.payments.bff.domain.request.Customer;
import com.enset.payments.bff.domain.request.Order;
import com.enset.payments.bff.domain.response.GetSessionResponse;
import com.enset.payments.bff.redis.CustomerEntity;
import com.enset.payments.bff.redis.OrderEntity;
import com.enset.payments.bff.redis.SessionEntity;

@Component
public class SessionEntityMapper {

    public GetSessionResponse fromEntity(SessionEntity sessionEntity) {
        return new GetSessionResponse(
                sessionEntity.getCheckoutUrl(),
                sessionEntity.getCallbackUrl(),
                sessionEntity.getSessionId(),
                sessionEntity.getTenant(),
                sessionEntity.getExpirationTime(),
                sessionEntity.getPaymentMethodType(),
                map(sessionEntity.getCustomer()),
                map(sessionEntity.getOrder()));
    }

    private Customer map(CustomerEntity entity) {
        return new Customer(entity.getCustomerId(), entity.getName(), entity.getEmail(), entity.getAddress(), entity.getPhone());
    }

    private Order map(OrderEntity entity) {
        return new Order(entity.getOrderId(), entity.getDescription(), entity.getAmount(),
                entity.getQuantity(), entity.getImage());
    }
}
