package com.enset.payments.bff.domain.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public record AuthorizeAndCaptureRequest(
        @NotBlank @Pattern(regexp = "(CREDIT_DEBIT_CARD|APPLE_PAY|PAYPAL)") String type,
        @NotBlank @Size(min = 32, max = 32) String oneTimeToken) {
}
