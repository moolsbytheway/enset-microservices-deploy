package com.enset.payments.bff.domain;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

import com.enset.payments.bff.domain.request.CreateSessionRequest;
import com.enset.payments.bff.redis.CustomerEntity;
import com.enset.payments.bff.redis.OrderEntity;
import com.enset.payments.bff.redis.SessionEntity;
import com.enset.payments.bff.service.JWTUtil;

public class SessionFactory {
    public static SessionEntity newPaymentSession(CreateSessionRequest request, String tenant, Integer sessionExpirationTimeInMinutes) {
        final String sessionId = generateUUID();
        final String customerId = generateUUID();
        final CustomerEntity customer = new CustomerEntity(customerId, request.customer().name(), request.customer().email(),
                request.customer().address(), request.customer().phone());
        final OrderEntity order = new OrderEntity(request.order().orderId(), request.order().description(), request.order().amount(), request.order().quantity(), request.order().image());
        return new SessionEntity(request.checkoutUrl(), request.callbackUrl(), sessionId, request.paymentMethodType(), customer, order,
                tenant,
                Instant.now().plus(Duration.ofMinutes(sessionExpirationTimeInMinutes)));
    }

    public static String newPaymentUrl(String sessionId, String frontendUrl, Integer sessionExpirationTimeInMinutes) {
        String sessionToken = JWTUtil.generateToken(sessionId, Duration.ofMinutes(sessionExpirationTimeInMinutes));
        return String.format("%s?session_token=%s", frontendUrl, sessionToken);
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
    }
}
