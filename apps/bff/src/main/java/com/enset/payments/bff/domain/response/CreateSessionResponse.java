package com.enset.payments.bff.domain.response;


public record CreateSessionResponse(
        String paymentUrl) {
}
