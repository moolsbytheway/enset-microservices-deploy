package com.enset.payments.bff.apikey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@ConditionalOnMissingBean(ApiKeyResolver.class)
public class DummyApiKeyResolver implements IApiKeyResolver {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    public void assertApiKeyIsValidForScope(ApiKeyScope apiKeyScope, String apiKeyBase64String) {
        LOG.info("Using dummy api key");
    }
}
