package com.enset.payments.bff.service;

import java.time.Duration;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

public class JWTUtil {

    // Replace with your secret key
    private static final String SECRET = "cesh7$Ob70-4eeb1jé@fM_à*1f5-0b87583dfs";
    private static final Algorithm ALGORITHM = Algorithm.HMAC256(SECRET);

    /**
     * Generates a JWT token.
     *
     * @param subject The subject of the token.
     * @return A signed JWT token.
     */
    public static String generateToken(String subject, Duration expiration) {
        return JWT.create()
                .withSubject(subject)
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + expiration.toMillis()))
                .sign(ALGORITHM);
    }

    /**
     * Parses a JWT token.
     *
     * @param token The JWT token to parse.
     * @return The decoded JWT.
     * @throws Exception if the token is invalid or expired.
     */
    public static DecodedJWT parseToken(String token) {
        JWTVerifier verifier = JWT.require(ALGORITHM).build();
        return verifier.verify(token);
    }

    /**
     * Extracts the subject from a JWT token.
     *
     * @param token The JWT token.
     * @return The subject of the token.
     * @throws Exception if the token is invalid or expired.
     */
    public static String getSubject(String token) {
        DecodedJWT jwt = parseToken(token);
        return jwt.getSubject();
    }
}
