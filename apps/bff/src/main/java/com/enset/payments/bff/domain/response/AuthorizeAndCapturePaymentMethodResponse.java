package com.enset.payments.bff.domain.response;

public record AuthorizeAndCapturePaymentMethodResponse(
        String type,
        Double amount) {
}
