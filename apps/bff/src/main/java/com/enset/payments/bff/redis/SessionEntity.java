package com.enset.payments.bff.redis;

import java.time.Instant;

public class SessionEntity {
    private String checkoutUrl;
    private String callbackUrl;
    private String sessionId;
    private CustomerEntity customer;
    private OrderEntity order;
    private String paymentMethodType;
    private String tenant;
    private Instant expirationTime;

    public SessionEntity(String checkoutUrl, String callbackUrl, String sessionId, String paymentMethodType, CustomerEntity customer, OrderEntity order, String tenant, Instant expirationTime) {
        this.checkoutUrl = checkoutUrl;
        this.callbackUrl = callbackUrl;
        this.sessionId = sessionId;
        this.customer = customer;
        this.order = order;
        this.paymentMethodType = paymentMethodType;
        this.tenant = tenant;
        this.expirationTime = expirationTime;
    }

    public SessionEntity() {
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Instant getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Instant expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getCheckoutUrl() {
        return checkoutUrl;
    }

    public void setCheckoutUrl(String checkoutUrl) {
        this.checkoutUrl = checkoutUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
}
