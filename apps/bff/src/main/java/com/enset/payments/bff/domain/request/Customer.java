package com.enset.payments.bff.domain.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public record Customer(
        String customerId,
        @NotBlank @Size(min = 2, max = 64) String name,
        @NotBlank @Email @Size(min = 3, max = 64) String email,
        @NotBlank @Size(min = 2, max = 64) String address,
        @NotBlank @Pattern(regexp = "\\d+", message = "Invalid phone number")
        @Size(min = 10, max = 10) String phone
) {
}
