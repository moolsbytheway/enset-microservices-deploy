package com.enset.payments.bff.domain.response;


import java.time.Instant;

import com.enset.payments.bff.domain.request.Customer;
import com.enset.payments.bff.domain.request.Order;

public record GetSessionResponse(
        String checkoutUrl,
        String callbackUrl,
        String sessionId,
        String tenant,
        Instant expirationTime,
        String paymentMethodType,
        Customer customer,
        Order order) {
}
