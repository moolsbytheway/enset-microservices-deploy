package com.enset.payments.bff.apikey;

import java.util.Base64;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.enset.payments.common.config.AwsSecrets;
import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;

@Component
@Order(3)
@ConditionalOnProperty(prefix = "common.awssecrets",
        name = "enabled",
        havingValue = "true")
public class ApiKeyResolver implements IApiKeyResolver {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    private final AwsSecrets awsSecrets;

    public ApiKeyResolver(AwsSecrets awsSecrets) {
        this.awsSecrets = awsSecrets;
    }

    public void assertApiKeyIsValidForScope(ApiKeyScope apiKeyScope, String apiKeyBase64String) {
        // apikey.refund.demo-store.camelcodes.net:z3OddqWT7f6FKbdQnY4qb4fmyifu9npL
        // apikey.SCOPE.DOMAIN:API_KEY

        String providedApiKeyDomain;
        String providedApiKeyValue;

        ApiKeyScope providedKeyScope;
        boolean validScope;

        try {
            String providedApiKeyString = new String(Base64.getDecoder().decode(apiKeyBase64String.getBytes()));
            providedApiKeyDomain = providedApiKeyString.split(":")[0];
            providedApiKeyValue = providedApiKeyString.split(":")[1];

            providedKeyScope = ApiKeyScope.valueOf(providedApiKeyDomain.split("\\.")[1].toUpperCase());
            validScope = apiKeyScope.equals(providedKeyScope);

        } catch (Exception e) {
            LOG.error("Couldn't decode API key", e);
            throw new BusinessException(ErrorCodes.INVALID_API_KEY);
        }


        if (!validScope) {
            LOG.error("Couldn't find provided API key in database: {}", providedApiKeyDomain);
            throw new BusinessException(ErrorCodes.INVALID_API_KEY_SCOPE);
        }

        Optional<String> optionalActualApiKeyValue = awsSecrets.getApiKey(providedApiKeyDomain);


        if (optionalActualApiKeyValue.isEmpty() ||
                !providedApiKeyValue.equals(optionalActualApiKeyValue.get())) {
            LOG.error("Couldn't find provided API key in database: {}", providedApiKeyDomain);
            throw new BusinessException(ErrorCodes.INVALID_API_KEY);
        }
    }
}
