package com.enset.payments.bff.service;

import static com.enset.payments.common.logger.ServiceKeys.PROCESSOR;
import static com.enset.payments.common.logger.ServiceKeys.TOKENIZATION;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enset.payments.bff.domain.request.AuthorizeAndCaptureRequest;
import com.enset.payments.bff.domain.request.TokenizationRequest;
import com.enset.payments.bff.domain.response.AuthorizeAndCaptureResponse;
import com.enset.payments.bff.domain.response.TokenizationResponse;
import com.enset.payments.bff.feign.processor.ProcessorClient;
import com.enset.payments.bff.feign.processor.request.ExternalAuthorizeAndCapturePaymentMethod;
import com.enset.payments.bff.feign.processor.request.ExternalAuthorizeAndCaptureRequest;
import com.enset.payments.bff.feign.processor.request.PaymentReferenceRequest;
import com.enset.payments.bff.feign.refund.RefundExecutorClient;
import com.enset.payments.bff.feign.tokenization.TokenizationClient;
import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;

@Service
public class CreditCardService {
    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    private final OutboundLogger outboundLogger;
    private final InboundLogger inboundLogger;
    private final SessionService sessionService;
    private final RefundExecutorClient refundExecutorClient;
    private final ProcessorClient processorClient;
    private final TokenizationClient tokenizationClient;

    public CreditCardService(OutboundLogger outboundLogger, InboundLogger inboundLogger, SessionService sessionService, RefundExecutorClient refundExecutorClient, ProcessorClient processorClient, TokenizationClient tokenizationClient) {
        this.outboundLogger = outboundLogger;
        this.inboundLogger = inboundLogger;
        this.sessionService = sessionService;
        this.refundExecutorClient = refundExecutorClient;
        this.processorClient = processorClient;
        this.tokenizationClient = tokenizationClient;
    }

    @Transactional
    public AuthorizeAndCaptureResponse authorizeAndCapture(AuthorizeAndCaptureRequest request, String sessionToken) {

        var session = sessionService.getSession(sessionToken);

        var processorRequest = new ExternalAuthorizeAndCaptureRequest(
                session.customer().customerId(),
                new ExternalAuthorizeAndCapturePaymentMethod(request.type(), session.order().amount(), request.oneTimeToken()),
                session.order().orderId()
        );

        outboundLogger.info(PROCESSOR, "Authorize and capture", processorRequest);
        ResponseEntity<AuthorizeAndCaptureResponse> response = processorClient.authorizeAndCapture(processorRequest, session.tenant());
        inboundLogger.info(PROCESSOR, "Authorize and capture", response.getBody());
        sessionService.deleteSession(session.sessionId());

        return response.getBody();
    }

    public AuthorizeAndCaptureResponse verifyPaymentResult(PaymentReferenceRequest request, String tenant) {

        outboundLogger.info(PROCESSOR, "Verify payment result", request);
        ResponseEntity<AuthorizeAndCaptureResponse> response = processorClient.verifyPaymentResult(request, tenant);
        inboundLogger.info(PROCESSOR, "Verify payment result", response.getBody());

        return response.getBody();
    }

    public void refund(PaymentReferenceRequest request, String tenant) {
        outboundLogger.info(PROCESSOR, "Refund", request);
        refundExecutorClient.refund(request, tenant);
    }

    public void complete(PaymentReferenceRequest request, String tenant) {
        outboundLogger.info(PROCESSOR, "Complete", request);
        processorClient.complete(request, tenant);
    }

    public TokenizationResponse tokenize(TokenizationRequest request, String sessionToken) {
        sessionService.checkSessionValid(sessionToken);

        outboundLogger.info(TOKENIZATION, "Tokenize", request);
        ResponseEntity<TokenizationResponse> response = tokenizationClient.vault(request);
        inboundLogger.info(TOKENIZATION, "Tokenize", response.getBody());

        return response.getBody();
    }
}
