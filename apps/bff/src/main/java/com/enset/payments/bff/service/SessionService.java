package com.enset.payments.bff.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.enset.payments.bff.domain.SessionFactory;
import com.enset.payments.bff.domain.request.CreateSessionRequest;
import com.enset.payments.bff.domain.response.CreateSessionResponse;
import com.enset.payments.bff.domain.response.GetSessionResponse;
import com.enset.payments.bff.redis.SessionEntity;
import com.enset.payments.bff.redis.mapper.SessionEntityMapper;
import com.enset.payments.bff.redis.repository.SessionRedisRepository;
import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;

@Service
public class SessionService {
    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    @Value("${session.expiration-time-in-minutes}")
    private Integer sessionExpirationTimeInMinutes;
    @Value("${session.frontend-url}")
    private String frontendUrl;

    private final SessionRedisRepository sessionRedisRepository;
    private final SessionEntityMapper sessionEntityMapper;

    public SessionService(SessionEntityMapper sessionEntityMapper, SessionRedisRepository sessionRedisRepository) {
        this.sessionRedisRepository = sessionRedisRepository;
        this.sessionEntityMapper = sessionEntityMapper;
    }

    public CreateSessionResponse createSession(CreateSessionRequest request, String tenant) {

        final SessionEntity sessionEntity = SessionFactory.newPaymentSession(request, tenant, sessionExpirationTimeInMinutes);

        sessionRedisRepository.save(sessionEntity);

        String paymentUrl = SessionFactory.newPaymentUrl(sessionEntity.getSessionId(), frontendUrl,  sessionExpirationTimeInMinutes);

        return new CreateSessionResponse(paymentUrl);
    }

    public GetSessionResponse getSession(String sessionToken) {

        try {
            String sessionId = JWTUtil.getSubject(sessionToken);

            Optional<SessionEntity> sessionEntityOptional = sessionRedisRepository.findById(sessionId);

            if (sessionEntityOptional.isEmpty()) {
                throw new BusinessException(ErrorCodes.SESSION_EXPIRED);
            }
            final SessionEntity sessionEntity = sessionEntityOptional.get();

            return sessionEntityMapper.fromEntity(sessionEntity);

        } catch (Exception e) {
            LOG.error("Couldn't fetch session", e);
            throw new BusinessException(ErrorCodes.SESSION_EXPIRED);
        }
    }
    public void checkSessionValid(String sessionToken) {

        try {
            String sessionId = JWTUtil.getSubject(sessionToken);

            Optional<SessionEntity> sessionEntityOptional = sessionRedisRepository.findById(sessionId);

            if (sessionEntityOptional.isEmpty()) {
                throw new BusinessException(ErrorCodes.SESSION_EXPIRED);
            }

        } catch (Exception e) {
            LOG.error("Couldn't fetch session", e);
            throw new BusinessException(ErrorCodes.SESSION_EXPIRED);
        }
    }

    public void deleteSession(String sessionId) {
        sessionRedisRepository.deleteById(sessionId);
    }
}
