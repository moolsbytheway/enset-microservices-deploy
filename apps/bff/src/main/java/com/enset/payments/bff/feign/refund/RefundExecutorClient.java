package com.enset.payments.bff.feign.refund;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import com.enset.payments.bff.feign.processor.request.PaymentReferenceRequest;
import com.enset.payments.common.config.FeignConfiguration;


@FeignClient(name = "refundExecutorClient",
        url = "${clients.refund-executor.base-url}",
        configuration = FeignConfiguration.class)
public interface RefundExecutorClient {
    @PostMapping
    ResponseEntity<Void> refund(PaymentReferenceRequest request, @RequestHeader("X-Tenant") String tenant);
}
