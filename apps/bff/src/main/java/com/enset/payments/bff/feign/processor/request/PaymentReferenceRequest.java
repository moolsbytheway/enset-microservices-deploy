package com.enset.payments.bff.feign.processor.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public record PaymentReferenceRequest(@NotBlank @Size(min = 32, max = 32) String paymentReference) {
}
