package com.enset.payments.bff.controller;

public final class PathsDefinitions {

    public static final String PAYMENTS_V1 = "/api/v1/payments";
    public static final String ADD_CREDIT_CARD = "/add-credit-card";
    public static final String COMPLETE = "/complete";
    public static final String REFUND = "/refund";
    public static final String VERIFY_PAYMENT_RESULT = "/verify-payment-result";
    public static final String AUTHORIZE_AND_CAPTURE = "/authorize-and-capture";
    public static final String SESSIONS = "/sessions";

}
