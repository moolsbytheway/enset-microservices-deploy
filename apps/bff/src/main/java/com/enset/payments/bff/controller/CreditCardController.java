package com.enset.payments.bff.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enset.payments.bff.apikey.ApiKeyScope;
import com.enset.payments.bff.apikey.IApiKeyResolver;
import com.enset.payments.bff.apikey.TenantExtractor;
import com.enset.payments.bff.domain.request.AuthorizeAndCaptureRequest;
import com.enset.payments.bff.domain.request.TokenizationRequest;
import com.enset.payments.bff.domain.response.AuthorizeAndCaptureResponse;
import com.enset.payments.bff.domain.response.TokenizationResponse;
import com.enset.payments.bff.feign.processor.request.PaymentReferenceRequest;
import com.enset.payments.bff.service.CreditCardService;
import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;


@RestController
@RequestMapping(PathsDefinitions.PAYMENTS_V1)
public class CreditCardController {

    private final InboundLogger inboundLogger;
    private final OutboundLogger outboundLogger;


    private final CreditCardService creditCardService;
    private final IApiKeyResolver apiKeyResolver;

    public CreditCardController(
            InboundLogger inboundLogger, OutboundLogger outboundLogger, final CreditCardService creditCardService, IApiKeyResolver apiKeyResolver) {
        this.inboundLogger = inboundLogger;
        this.outboundLogger = outboundLogger;
        this.creditCardService = creditCardService;
        this.apiKeyResolver = apiKeyResolver;
    }


    @PostMapping(path = PathsDefinitions.AUTHORIZE_AND_CAPTURE)
    public ResponseEntity<AuthorizeAndCaptureResponse> authorizeAndCapture(
            @Valid @RequestBody final AuthorizeAndCaptureRequest request,
            @NotBlank @RequestHeader("Authorization") String sessionToken) {

        inboundLogger.info("Authorize and capture", request);

        final AuthorizeAndCaptureResponse response = creditCardService.authorizeAndCapture(request, sessionToken);

        outboundLogger.info("Authorize and capture", response);

        return ResponseEntity.ok(response);


    }

    @PostMapping(path = PathsDefinitions.ADD_CREDIT_CARD)
    public ResponseEntity<TokenizationResponse> tokenize(
            @NotBlank @RequestHeader("Authorization") String sessionToken,
            @Valid @RequestBody final TokenizationRequest request) {

        inboundLogger.info("Vault card", request);

        final TokenizationResponse response = creditCardService.tokenize(request, sessionToken);

        outboundLogger.info("Vault card", response);

        return ResponseEntity.ok(response);

    }

    @PostMapping(path = PathsDefinitions.COMPLETE)
    public ResponseEntity<Void> complete(
            @Valid @RequestBody PaymentReferenceRequest request,
            @NotBlank @RequestHeader("X-API-KEY") String apiKey) {
        inboundLogger.info("Complete payment", request);
        apiKeyResolver.assertApiKeyIsValidForScope(ApiKeyScope.SESSION, apiKey);

        String tenant = TenantExtractor.extractTenantFromApiKey(apiKey);
        creditCardService.complete(request, tenant);
        outboundLogger.info("Complete payment", null);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping(path = PathsDefinitions.VERIFY_PAYMENT_RESULT)
    public ResponseEntity<AuthorizeAndCaptureResponse> verifyPaymentResult(
            @Valid @RequestBody PaymentReferenceRequest request,
            @NotBlank @RequestHeader("X-API-KEY") String apiKey) {
        apiKeyResolver.assertApiKeyIsValidForScope(ApiKeyScope.SESSION, apiKey);
        inboundLogger.info("Verify payment result", request);
        String tenant = TenantExtractor.extractTenantFromApiKey(apiKey);
        AuthorizeAndCaptureResponse result = creditCardService.verifyPaymentResult(request, tenant);
        outboundLogger.info("Verify payment result", result);
        return ResponseEntity.ok(result);
    }

    @PostMapping(path = PathsDefinitions.REFUND)
    public ResponseEntity<Void> refund(
            @Valid @RequestBody PaymentReferenceRequest request,
            @NotBlank @RequestHeader("X-API-KEY") String apiKey) {
        inboundLogger.info("Refund payment", request);
        apiKeyResolver.assertApiKeyIsValidForScope(ApiKeyScope.REFUND, apiKey);
        String tenant = TenantExtractor.extractTenantFromApiKey(apiKey);
        creditCardService.refund(request, tenant);
        outboundLogger.info("Refund payment", null);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
