package com.enset.payments.bff.apikey;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;

public class TenantExtractor {

    private static final Logger LOG = LoggerFactory.getLogger(TenantExtractor.class.getName());

    public static String extractTenantFromApiKey(String apiKeyBase64String) {

        String providedApiKeyDomain;

        try {
            String providedApiKeyString = new String(Base64.getDecoder().decode(apiKeyBase64String.getBytes()));
            providedApiKeyDomain = extractDomain(providedApiKeyString);

        } catch (Exception e) {
            LOG.error("Couldn't decode API key", e);
            throw new BusinessException(ErrorCodes.INVALID_API_KEY);
        }

        return providedApiKeyDomain;
    }

    private static String extractDomain(String s) {
        int firstDotIndex = s.indexOf('.', "apikey.".length());
        int colonIndex = s.indexOf(':');
        return s.substring(firstDotIndex + 1, colonIndex);
    }
}
