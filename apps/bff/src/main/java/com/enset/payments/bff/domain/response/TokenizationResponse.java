package com.enset.payments.bff.domain.response;

public record TokenizationResponse(String oneTimeToken) {
}
