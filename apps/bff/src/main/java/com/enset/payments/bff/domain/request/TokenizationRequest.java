package com.enset.payments.bff.domain.request;

import java.util.UUID;

import com.enset.payments.common.validators.LuhnAlgorithmValid;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class TokenizationRequest {
    @NotBlank
    @LuhnAlgorithmValid
    private String pan;

    @NotBlank
    @Size(min = 4, max = 4)
    private String expiryYear;

    @NotBlank
    @Size(min = 1, max = 2)
    private String expiryMonth;

    @NotEmpty
    private String cardholderName;

    @Size(min = 3, max = 4)
    @NotBlank
    @Pattern(regexp = "\\d+", message = "Incorrect CVV format, must be number only")
    private String cvv;

    public TokenizationRequest() {
    }

    public TokenizationRequest(String pan, String expiryYear, String expiryMonth, String cardholderName, String cvv) {
        this.pan = pan;
        this.expiryYear = expiryYear;
        this.expiryMonth = expiryMonth;
        this.cardholderName = cardholderName;
        this.cvv = cvv;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
}
