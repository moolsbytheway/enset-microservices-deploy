package com.enset.payments.bff.redis.repository;


import static com.enset.payments.common.config.JacksonConfiguration.STATIC_OBJECT_MAPPER;

import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IRedisRepository<T, ID> {

    Optional<T> findById(ID id);

    void deleteById(ID id);

    void save(T entity);

    default T readValue(String strVal, Class<T> clazz) {
        try {
            return STATIC_OBJECT_MAPPER.readValue(strVal, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    default String writeValue(T obj) {
        try {
            return STATIC_OBJECT_MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
