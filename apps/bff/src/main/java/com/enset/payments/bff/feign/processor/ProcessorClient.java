package com.enset.payments.bff.feign.processor;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import com.enset.payments.bff.domain.response.AuthorizeAndCaptureResponse;
import com.enset.payments.bff.feign.processor.request.ExternalAuthorizeAndCaptureRequest;
import com.enset.payments.bff.feign.processor.request.PaymentReferenceRequest;
import com.enset.payments.common.config.FeignConfiguration;


@FeignClient(name = "processorClient",
        url = "${clients.processor.base-url}",
        configuration = FeignConfiguration.class)
public interface ProcessorClient {
    @PostMapping("/authorize-and-capture")
    ResponseEntity<AuthorizeAndCaptureResponse> authorizeAndCapture(ExternalAuthorizeAndCaptureRequest request,
                                                                    @RequestHeader("X-Tenant") String tenant);

    @PostMapping("/verify-payment-result")
    ResponseEntity<AuthorizeAndCaptureResponse> verifyPaymentResult(PaymentReferenceRequest request,
                                                                    @RequestHeader("X-Tenant") String tenant);

    @PostMapping("/complete")
    ResponseEntity<Void> complete(PaymentReferenceRequest request, @RequestHeader("X-Tenant") String tenant);
}
