package com.enset.payments.gateway;

import java.io.IOException;
import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;

@Component
@ConditionalOnProperty(prefix = "common.gateway",
        name = "log-requests",
        havingValue = "true")
public class LoggingFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(LoggingFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
        // You can add filter initialization code here if needed
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        // Collect request information
        StringBuilder logMessage = new StringBuilder();
        logMessage.append("{\"method\": \"").append(httpServletRequest.getMethod()).append("\", ");
        logMessage.append("\"request_url\": \"").append(httpServletRequest.getRequestURL()).append("\", ");

        // Collect headers information
        logMessage.append("\"headers\": {");
        Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = httpServletRequest.getHeader(headerName);
            logMessage.append("\"").append(headerName).append("\": \"").append(headerValue).append("\", ");
        }
        // Remove the trailing comma and space, and close the headers part
        if (logMessage.lastIndexOf(", ") == logMessage.length() - 2) {
            logMessage.delete(logMessage.length() - 2, logMessage.length());
        }
        logMessage.append("}}");

        // Log everything in a single line
        logger.info(logMessage.toString());

        // Continue with the next filter in the chain
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // You can add filter cleanup code here if needed
    }
}
