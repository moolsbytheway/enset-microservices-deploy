package com.enset.payments.tokenization.redis;

public class OneTimeTokenEntity {
    private String ott;
    private CardDetailsEntity cardDetails;

    public OneTimeTokenEntity(String ott, CardDetailsEntity cardDetails) {
        this.ott = ott;
        this.cardDetails = cardDetails;
    }

    public OneTimeTokenEntity() {
    }

    public String getOtt() {
        return ott;
    }

    public void setOtt(String ott) {
        this.ott = ott;
    }

    public CardDetailsEntity getCardDetails() {
        return cardDetails;
    }

    public void setCardDetails(CardDetailsEntity cardDetails) {
        this.cardDetails = cardDetails;
    }
}
