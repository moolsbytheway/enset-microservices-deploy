package com.enset.payments.tokenization.controller;

public final class PathsDefinitions {

    public static final String VAULT_PATH = "/vault";
    public static final String GET_CARD_DETAILS_PATH = "/get-card-details";

}
