package com.enset.payments.tokenization.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.enset.payments.common.config.AwsSecrets;

@Configuration
@ConditionalOnProperty(prefix = "common.awssecrets",
        name = "enabled",
        havingValue = "true")
public class RedisConfig {

    private final Environment env;
    private final AwsSecrets awsSecrets;

    public RedisConfig(Environment env, AwsSecrets awsSecrets) {
        this.env = env;
        this.awsSecrets = awsSecrets;
    }

    @Bean
    @Order(2)
    @DependsOn("awsSecrets")
    public JedisConnectionFactory jedisConnectionFactory() {
        var redisHost = env.getProperty("redis.host");
        var redisPort = env.getProperty("redis.port");
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName(redisHost);
        jedisConnectionFactory.setPort(Integer.parseInt(redisPort));
        jedisConnectionFactory.setPassword(awsSecrets.getRedisDbPassword());
        return jedisConnectionFactory;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        return redisTemplate;
    }
}
