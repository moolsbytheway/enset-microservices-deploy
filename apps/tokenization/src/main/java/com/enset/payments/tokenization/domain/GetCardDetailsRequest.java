package com.enset.payments.tokenization.domain;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public class GetCardDetailsRequest {
    @NotBlank
    @Size(min = 32, max = 32)
    private String ott;

    public GetCardDetailsRequest(String ott) {
        this.ott = ott;
    }

    public GetCardDetailsRequest() {
    }

    public String getOtt() {
        return ott;
    }

    public void setOtt(String ott) {
        this.ott = ott;
    }
}
