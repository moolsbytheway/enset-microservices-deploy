package com.enset.payments.tokenization.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;
import com.enset.payments.tokenization.domain.GetCardDetailsRequest;
import com.enset.payments.tokenization.domain.GetCardDetailsResponse;
import com.enset.payments.tokenization.domain.TokenizationRequest;
import com.enset.payments.tokenization.domain.TokenizationResponse;
import com.enset.payments.tokenization.service.TokenizationService;

import jakarta.validation.Valid;


@RestController
@RequestMapping("/api/v1/tokenization")
public class TokenizationController {

    private final TokenizationService tokenizationService;
    private final InboundLogger inboundLogger;
    private final OutboundLogger outboundLogger;

    public TokenizationController(
            final TokenizationService tokenizationService, InboundLogger inboundLogger, OutboundLogger outboundLogger) {
        this.tokenizationService = tokenizationService;
        this.inboundLogger = inboundLogger;
        this.outboundLogger = outboundLogger;
    }

    @PostMapping(path = PathsDefinitions.VAULT_PATH)
    public ResponseEntity<TokenizationResponse> vault(
            @Valid @RequestBody final TokenizationRequest request) {

        inboundLogger.info("Vault card", request);

        final TokenizationResponse response = tokenizationService.vault(request);

        outboundLogger.info("Vault card", response);

        return ResponseEntity.ok(response);
    }

    @PostMapping(path = PathsDefinitions.GET_CARD_DETAILS_PATH)
    public ResponseEntity<GetCardDetailsResponse> getCardDetails(
            @Valid @RequestBody final GetCardDetailsRequest request) {

        inboundLogger.info("Get card details", request);

        GetCardDetailsResponse response = tokenizationService.get(request);

        outboundLogger.info("Get card details", response);

        return ResponseEntity.ok(response);
    }
}
