package com.enset.payments.tokenization.domain;

public record TokenizationResponse(String oneTimeToken) {
}
