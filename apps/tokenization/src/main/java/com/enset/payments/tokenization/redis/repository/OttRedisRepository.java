package com.enset.payments.tokenization.redis.repository;


import static com.enset.payments.common.logger.ServiceKeys.REDIS;

import java.time.Duration;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;
import com.enset.payments.tokenization.redis.OneTimeTokenEntity;

@Repository
public class OttRedisRepository implements IRedisRepository<OneTimeTokenEntity, String> {

    @Value("${one-time-token.expiration-time-in-minutes}")
    private Integer ottExpirationTimeInMinutes;

    private final StringRedisTemplate redisTemplate;
    private final InboundLogger inboundLogger;
    private final OutboundLogger outboundLogger;

    public OttRedisRepository(StringRedisTemplate redisTemplate, InboundLogger inboundLogger, OutboundLogger outboundLogger) {
        this.redisTemplate = redisTemplate;
        this.inboundLogger = inboundLogger;
        this.outboundLogger = outboundLogger;
    }


    public Optional<OneTimeTokenEntity> findById(String id) {
        String redisKey = getKey(id);

        if (!Boolean.TRUE.equals(redisTemplate.hasKey(redisKey))) return Optional.empty();

        outboundLogger.info(REDIS, "GET Card ", redisKey);
        String redisValue = redisTemplate.opsForValue().get(redisKey);
        inboundLogger.info(REDIS, "GET Card ", redisKey);
        return Optional.ofNullable(readValue(redisValue, OneTimeTokenEntity.class));
    }

    public void deleteById(String id) {
        String redisKey = getKey(id);

        if (Boolean.TRUE.equals(redisTemplate.hasKey(redisKey))) {
            outboundLogger.info(REDIS, "DELETE Card", redisKey);
            redisTemplate.delete(redisKey);
        }

    }

    public void save(OneTimeTokenEntity entity) {
        String redisKey = getKey(entity.getOtt());

        outboundLogger.info(REDIS, "Vault card", redisKey);
        redisTemplate.opsForValue().set(redisKey, writeValue(entity), Duration.ofMinutes(ottExpirationTimeInMinutes));
    }

    public String getKey(String id) {
        return "tokenization.ott." + id;
    }

}
