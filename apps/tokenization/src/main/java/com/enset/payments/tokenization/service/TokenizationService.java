package com.enset.payments.tokenization.service;

import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;
import com.enset.payments.tokenization.domain.GetCardDetailsRequest;
import com.enset.payments.tokenization.domain.GetCardDetailsResponse;
import com.enset.payments.tokenization.domain.TokenizationRequest;
import com.enset.payments.tokenization.domain.TokenizationResponse;
import com.enset.payments.tokenization.redis.CardDetailsEntity;
import com.enset.payments.tokenization.redis.OneTimeTokenEntity;
import com.enset.payments.tokenization.redis.repository.OttRedisRepository;

@Service
public class TokenizationService {
    private final OttRedisRepository ottRedisRepository;

    public TokenizationService(OttRedisRepository ottRedisRepository) {
        this.ottRedisRepository = ottRedisRepository;
    }

    public TokenizationResponse vault(TokenizationRequest request) {
        String oneTimeToken = generateUUID();
        OneTimeTokenEntity ottRedisEntity = new OneTimeTokenEntity(oneTimeToken,
                new CardDetailsEntity(request.getPan(), request.getExpiryYear(), request.getExpiryMonth(),
                        request.getCardholderName(), request.getCvv()));
        ottRedisRepository.save(ottRedisEntity);
        return new TokenizationResponse(oneTimeToken);
    }

    public GetCardDetailsResponse get(GetCardDetailsRequest request) {
        Optional<OneTimeTokenEntity> ottEntityOptional = ottRedisRepository.findById(request.getOtt());
        if (ottEntityOptional.isEmpty()) {
            throw new BusinessException(ErrorCodes.OTT_EXPIRED);
        }
        OneTimeTokenEntity ottEntity = ottEntityOptional.get();
        ottRedisRepository.deleteById(request.getOtt());
        return new GetCardDetailsResponse(ottEntity.getCardDetails().getPan(),
                ottEntity.getCardDetails().getExpiryYear(),
                ottEntity.getCardDetails().getExpiryMonth(),
                ottEntity.getCardDetails().getCardholderName());
    }

    private static String generateUUID() {
        return UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
    }
}
