package com.enset.payments.tokenization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.enset.payments"})
public class TokenizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(TokenizationApplication.class, args);
    }

}
