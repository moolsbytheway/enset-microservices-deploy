package com.enset.payments.tokenization.domain;

public record GetCardDetailsResponse(String pan, String expiryYear, String expiryMonth, String cardholderName) {
}
