package com.enset.payments.tokenization.redis;

public class CardDetailsEntity {
    private String pan;

    private String expiryYear;

    private String expiryMonth;

    private String cardholderName;

    private String cvv;

    public CardDetailsEntity() {
    }

    public CardDetailsEntity(String pan, String expiryYear, String expiryMonth, String cardholderName, String cvv) {
        this.pan = pan;
        this.expiryYear = expiryYear;
        this.expiryMonth = expiryMonth;
        this.cardholderName = cardholderName;
        this.cvv = cvv;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
}
