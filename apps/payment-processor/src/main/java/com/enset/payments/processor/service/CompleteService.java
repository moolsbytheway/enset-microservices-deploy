package com.enset.payments.processor.service;

import java.time.Instant;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;
import com.enset.payments.processor.domain.TransactionStatus;
import com.enset.payments.processor.domain.request.PaymentReferenceRequest;
import com.enset.payments.processor.jpa.TransactionDocument;
import com.enset.payments.processor.repository.TransactionRepository;
import com.enset.payments.processor.tenant.TenantValidator;

@Component
public class CompleteService {

    private final TransactionRepository transactionRepository;
    private final TenantValidator tenantValidator;

    public CompleteService(TransactionRepository transactionRepository, TenantValidator tenantValidator) {
        this.transactionRepository = transactionRepository;
        this.tenantValidator = tenantValidator;
    }


    public void complete(PaymentReferenceRequest request, String tenant) {
        Optional<TransactionDocument> entity = transactionRepository.findTransactionDocumentByPaymentRef(request.paymentReference());
        if (entity.isEmpty()) {
            throw new BusinessException(ErrorCodes.TRANSACTION_MISSING);
        }

        tenantValidator.validate(tenant, entity.get().getTenant());

        if (TransactionStatus.CAPTURE_FAILURE.equals(entity.get().getStatus())) {
            throw new BusinessException(ErrorCodes.PAYMENT_FAILED_BEFORE_COMPLETE);
        }
        if (!TransactionStatus.CAPTURE_SUCCESS.equals(entity.get().getStatus())) {
            throw new BusinessException(ErrorCodes.TRANSACTION_COMPLETE_OR_REFUNDED);
        }

        transactionRepository.updateTransactionStatus(request.paymentReference(), TransactionStatus.COMPLETED, Instant.now());

    }
}
