package com.enset.payments.processor.feign.tokenization;

public record GetCardDetailsRequest(String ott) {
}
