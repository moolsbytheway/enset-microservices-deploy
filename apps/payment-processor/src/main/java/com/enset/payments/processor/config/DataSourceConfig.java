package com.enset.payments.processor.config;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.enset.payments.common.config.AwsSecrets;

@Configuration
@ConditionalOnProperty(prefix = "common.awssecrets",
        name = "enabled",
        havingValue = "true")
public class DataSourceConfig {

    private final Environment env;
    private final AwsSecrets awsSecrets;

    public DataSourceConfig(Environment env, AwsSecrets awsSecrets) {
        this.env = env;
        this.awsSecrets = awsSecrets;
    }

    @Bean
    @Order(2)
    @DependsOn("awsSecrets")
    public DataSource dataSource() {
        var host = env.getProperty("database.host");
        var username = awsSecrets.getMariaDbUsername();
        var password = awsSecrets.getMariaDbPassword();
        var database = awsSecrets.getMariaDbDatabaseName();

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.mariadb.jdbc.Driver");
        dataSource.setUrl(String.format("jdbc:mariadb://%s/%s", host, database));
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }
}
