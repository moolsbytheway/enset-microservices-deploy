package com.enset.payments.processor.domain.response;

import java.time.Instant;

import com.enset.payments.processor.domain.TransactionStatus;

public record AuthorizeAndCaptureResponse(
        TransactionStatus status,
        String paymentReference,
        String tenant,
        Instant dateTime,
        AuthorizeAndCapturePaymentMethodResponse paymentMethod) {
}
