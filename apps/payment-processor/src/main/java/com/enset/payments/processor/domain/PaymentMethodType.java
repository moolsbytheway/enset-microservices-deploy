package com.enset.payments.processor.domain;

public enum PaymentMethodType {
    CREDIT_DEBIT_CARD,
    APPLE_PAY,
    PAYPAL
}
