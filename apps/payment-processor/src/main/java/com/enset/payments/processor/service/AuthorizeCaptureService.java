package com.enset.payments.processor.service;

import static com.enset.payments.common.logger.ServiceKeys.TOKENIZATION;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;
import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;
import com.enset.payments.processor.domain.PaymentMethodType;
import com.enset.payments.processor.domain.TransactionStatus;
import com.enset.payments.processor.domain.request.AuthorizeAndCaptureRequest;
import com.enset.payments.processor.domain.response.AuthorizeAndCaptureResponse;
import com.enset.payments.processor.feign.tokenization.GetCardDetailsRequest;
import com.enset.payments.processor.feign.tokenization.GetCardDetailsResponse;
import com.enset.payments.processor.feign.tokenization.TokenizationClient;
import com.enset.payments.processor.jpa.TransactionDocument;
import com.enset.payments.processor.jpa.mapper.TransactionDocumentMapper;
import com.enset.payments.processor.repository.TransactionRepository;
import com.enset.payments.processor.tenant.TenantValidator;

@Component
public class AuthorizeCaptureService {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    private final TenantValidator tenantValidator;
    private final TransactionRepository transactionRepository;
    private final TransactionDocumentMapper transactionDocumentMapper;
    private final TokenizationClient tokenizationClient;
    private final OutboundLogger outboundLogger;
    private final InboundLogger inboundLogger;

    public AuthorizeCaptureService(TenantValidator tenantValidator, TransactionDocumentMapper transactionDocumentMapper,
                                   TransactionRepository transactionRepository,
                                   TokenizationClient tokenizationClient, OutboundLogger outboundLogger, InboundLogger inboundLogger) {
        this.tenantValidator = tenantValidator;
        this.transactionDocumentMapper = transactionDocumentMapper;
        this.transactionRepository = transactionRepository;
        this.tokenizationClient = tokenizationClient;
        this.outboundLogger = outboundLogger;
        this.inboundLogger = inboundLogger;
    }

    public AuthorizeAndCaptureResponse authorizeAndCapture(AuthorizeAndCaptureRequest request, String tenant) {
        final String paymentRef = generateUUID();

        // make sure card token exists
        if (PaymentMethodType.CREDIT_DEBIT_CARD.name().equals(request.paymentMethod().type())) {

            outboundLogger.info(TOKENIZATION, "Get card details", request.paymentMethod().oneTimeToken());

            final ResponseEntity<GetCardDetailsResponse> response = tokenizationClient
                    .getCardDetails(new GetCardDetailsRequest(request.paymentMethod().oneTimeToken()));

            inboundLogger.info(TOKENIZATION, "Get card details", response.getBody());
        }

        // mock failure status when order id is 000000
        TransactionStatus status = request.orderId().startsWith("0") ? TransactionStatus.CAPTURE_FAILURE : TransactionStatus.CAPTURE_SUCCESS;

        final TransactionDocument entity = new TransactionDocument(null,
                paymentRef,
                request.orderId(), request.customerId(),
                PaymentMethodType.valueOf(request.paymentMethod().type()),
                request.paymentMethod().amount(),
                status,
                tenant,
                Instant.now());

        transactionRepository.save(entity);

        return transactionDocumentMapper.fromEntity(entity);

    }

    public AuthorizeAndCaptureResponse verifyPaymentResult(String paymentReference, String tenant) {
        Optional<TransactionDocument> entity = transactionRepository.findTransactionDocumentByPaymentRef(paymentReference);
        if (entity.isEmpty()) {
            throw new BusinessException(ErrorCodes.TRANSACTION_MISSING);
        }
        tenantValidator.validate(tenant, entity.get().getTenant());

        return transactionDocumentMapper.fromEntity(entity.get());

    }

    private static String generateUUID() {
        return UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
    }
}
