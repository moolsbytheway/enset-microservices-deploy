package com.enset.payments.processor.domain.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;

public record AuthorizeAndCapturePaymentMethod(
        @NotNull @Pattern(regexp = "(CREDIT_DEBIT_CARD|APPLE_PAY|PAYPAL)") String type,
        @NotNull @Positive Double amount,
        @NotBlank @Size(min = 32, max = 32) String oneTimeToken) {
}
