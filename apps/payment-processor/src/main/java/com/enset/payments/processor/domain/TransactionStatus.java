package com.enset.payments.processor.domain;

public enum TransactionStatus {
    CAPTURE_SUCCESS,
    CAPTURE_FAILURE,
    COMPLETED,
    REFUNDED
}
