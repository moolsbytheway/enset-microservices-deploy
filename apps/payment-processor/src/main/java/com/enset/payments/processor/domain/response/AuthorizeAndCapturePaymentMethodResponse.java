package com.enset.payments.processor.domain.response;

import com.enset.payments.processor.domain.PaymentMethodType;

public record AuthorizeAndCapturePaymentMethodResponse(
        PaymentMethodType type,
        Double amount) {
}
