package com.enset.payments.processor.feign.tokenization;

public record GetCardDetailsResponse(String pan, Integer expiryYear, Integer expiryMonth, String cardholderName) {
}
