package com.enset.payments.processor.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enset.payments.common.logger.InboundLogger;
import com.enset.payments.common.logger.OutboundLogger;
import com.enset.payments.processor.domain.request.AuthorizeAndCaptureRequest;
import com.enset.payments.processor.domain.request.PaymentReferenceRequest;
import com.enset.payments.processor.domain.response.AuthorizeAndCaptureResponse;
import com.enset.payments.processor.service.AuthorizeCaptureService;
import com.enset.payments.processor.service.CompleteService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;


@RestController
@RequestMapping("/api/v1/processor")
public class AuthorizeAndCaptureController {

    private final AuthorizeCaptureService authorizeCaptureService;
    private final CompleteService completeService;
    private final InboundLogger inboundLogger;
    private final OutboundLogger outboundLogger;

    public AuthorizeAndCaptureController(
            final AuthorizeCaptureService authorizeCaptureService,
            final CompleteService completeService, InboundLogger inboundLogger, OutboundLogger outboundLogger
    ) {
        this.authorizeCaptureService = authorizeCaptureService;
        this.completeService = completeService;
        this.inboundLogger = inboundLogger;
        this.outboundLogger = outboundLogger;
    }

    @PostMapping(path = PathsDefinitions.AUTHORIZE_AND_CAPTURE_PATH)
    public ResponseEntity<AuthorizeAndCaptureResponse> authorizeAndCapture(
            @Valid @RequestBody final AuthorizeAndCaptureRequest request,
            @NotBlank @RequestHeader("X-Tenant") String tenant) {

        inboundLogger.info("Authorize and capture", request);

        final AuthorizeAndCaptureResponse response = authorizeCaptureService.authorizeAndCapture(request, tenant);

        outboundLogger.info("Authorize and capture", response);
        return ResponseEntity.ok(response);


    }

    @PostMapping(path = PathsDefinitions.VERIFY_PAYMENT_RESULT)
    public ResponseEntity<AuthorizeAndCaptureResponse> verifyPaymentResult(
            @Valid @RequestBody final PaymentReferenceRequest request,
            @NotBlank @RequestHeader("X-Tenant") String tenant) {

        inboundLogger.info("Verify payment result", request);

        final AuthorizeAndCaptureResponse response = authorizeCaptureService.verifyPaymentResult(request.paymentReference(), tenant);

        outboundLogger.info("Verify payment result", response);
        return ResponseEntity.ok(response);
    }

    @PostMapping(path = PathsDefinitions.COMPLETE)
    public ResponseEntity<Void> complete(
            @Valid @RequestBody final PaymentReferenceRequest request,
            @NotBlank @RequestHeader("X-Tenant") String tenant) {
        inboundLogger.info("Complete", request);

        completeService.complete(request, tenant);

        outboundLogger.info("Complete", null);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();


    }
}
