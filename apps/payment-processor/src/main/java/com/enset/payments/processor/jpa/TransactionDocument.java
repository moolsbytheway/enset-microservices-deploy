package com.enset.payments.processor.jpa;

import java.time.Instant;

import com.enset.payments.processor.domain.PaymentMethodType;
import com.enset.payments.processor.domain.TransactionStatus;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class TransactionDocument {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String paymentRef;
    private String orderId;
    private String customerId;
    @Enumerated(EnumType.STRING)
    private PaymentMethodType paymentMethodType;
    private Double amount;
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;
    private String tenant;
    private Instant createdAt;
    private Instant lastUpdatedAt;

    public TransactionDocument() {
    }

    public TransactionDocument(Long id, String paymentRef, String orderId, String customerId,
                               PaymentMethodType paymentMethodType, Double amount, TransactionStatus status, String tenant, Instant createdAt) {
        this.id = id;
        this.paymentRef = paymentRef;
        this.orderId = orderId;
        this.customerId = customerId;
        this.paymentMethodType = paymentMethodType;
        this.amount = amount;
        this.status = status;
        this.tenant = tenant;
        this.createdAt = createdAt;
        this.lastUpdatedAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public PaymentMethodType getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(PaymentMethodType paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public String getPaymentRef() {
        return paymentRef;
    }

    public void setPaymentRef(String paymentRef) {
        this.paymentRef = paymentRef;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Instant lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
}
