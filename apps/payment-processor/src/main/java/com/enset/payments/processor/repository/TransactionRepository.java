package com.enset.payments.processor.repository;

import java.time.Instant;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enset.payments.processor.domain.TransactionStatus;
import com.enset.payments.processor.jpa.TransactionDocument;

import jakarta.transaction.Transactional;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionDocument, Long> {

    Optional<TransactionDocument> findTransactionDocumentByPaymentRef(String paymentRef);

    @Query("update TransactionDocument t set t.status = ?2, t.lastUpdatedAt = ?3 where t.paymentRef = ?1")
    @Modifying
    @Transactional
    void updateTransactionStatus(String paymentRef, TransactionStatus status, Instant lastUpdatedAt);

}
