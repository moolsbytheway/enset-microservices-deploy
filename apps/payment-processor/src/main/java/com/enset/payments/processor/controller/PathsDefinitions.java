package com.enset.payments.processor.controller;

public final class PathsDefinitions {
    public static final String AUTHORIZE_AND_CAPTURE_PATH = "/authorize-and-capture";
    public static final String COMPLETE = "/complete";
    public static final String VERIFY_PAYMENT_RESULT = "/verify-payment-result";

}
