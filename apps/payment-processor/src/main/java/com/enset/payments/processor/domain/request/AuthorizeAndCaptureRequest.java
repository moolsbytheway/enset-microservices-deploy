package com.enset.payments.processor.domain.request;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public record AuthorizeAndCaptureRequest(
        @NotBlank String customerId,
        @Valid AuthorizeAndCapturePaymentMethod paymentMethod,
        @NotBlank @Size(min = 6, max = 6) String orderId) {
}
