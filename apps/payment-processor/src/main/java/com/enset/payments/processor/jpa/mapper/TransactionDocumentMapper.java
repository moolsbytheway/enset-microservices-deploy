package com.enset.payments.processor.jpa.mapper;

import org.springframework.stereotype.Component;

import com.enset.payments.processor.domain.response.AuthorizeAndCapturePaymentMethodResponse;
import com.enset.payments.processor.domain.response.AuthorizeAndCaptureResponse;
import com.enset.payments.processor.jpa.TransactionDocument;

@Component
public class TransactionDocumentMapper {

    public AuthorizeAndCaptureResponse fromEntity(TransactionDocument entity) {

        return new AuthorizeAndCaptureResponse(entity.getStatus(), entity.getPaymentRef(), entity.getTenant(), entity.getCreatedAt(),
                new AuthorizeAndCapturePaymentMethodResponse(entity.getPaymentMethodType(), entity.getAmount())
        );
    }
}
