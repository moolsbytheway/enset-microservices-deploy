package com.enset.payments.processor.tenant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.enset.payments.common.exception.BusinessException;
import com.enset.payments.common.exception.ErrorCodes;

@Component
public class TenantValidator {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

    public void validate(String providedTenant, String transactionDocumentTenant) {
        if (providedTenant == null || providedTenant.isBlank()) {
            LOG.error("Empty tenant");
            throw new BusinessException(ErrorCodes.INVALID_TENANT);
        }
        if (!providedTenant.equalsIgnoreCase(transactionDocumentTenant)) {
            LOG.error("Invalid tenant {} , transaction document tenant: {}", providedTenant, transactionDocumentTenant);
            throw new BusinessException(ErrorCodes.INVALID_TENANT);
        }
    }
}
