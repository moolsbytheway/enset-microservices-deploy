package com.enset.payments.processor.feign.tokenization;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import com.enset.payments.common.config.FeignConfiguration;


@FeignClient(name = "tokenizationClient",
        url = "${clients.tokenization.base-url}",
        configuration = FeignConfiguration.class)
public interface TokenizationClient {
    @PostMapping("/get-card-details")
    ResponseEntity<GetCardDetailsResponse> getCardDetails(GetCardDetailsRequest request);
}
