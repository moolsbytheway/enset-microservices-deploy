import GlobalStyle from "./components/GlobalStyle.ts";
import CreditCardForm from "./components/CreditCardForm.tsx";
import {Column, Row} from "./components/Components.tsx";
import Product from "./components/Product.tsx";
import {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import Countdown, {calculateInitialTime} from "./components/Countdown.tsx";
import BackToCheckOut from "./components/BackToCheckout.tsx";
import {backendUrl} from "./components/Config.ts";



function App() {
    const [sessionToken, setSessionToken] = useState("")
    const [invalidToken, setInvalidToken] = useState(false)
    const [sessionData, setSessionData] = useState(null)
    const [countdownMinutes, setCountdownMinutes] = useState(-1)
    const [countdownSeconds, setCountdownSeconds] = useState(-1)
    const [initCountdown, setInitCountdown] = useState(false)
    const [sessionExpired, setSessionExpired] = useState(false)
    const [validSession, setValidSession] = useState(false)

    const location = useLocation();

    useEffect(() => {
        setValidSession(!sessionExpired && !invalidToken && sessionData != null)
    }, [sessionExpired, invalidToken, sessionData]);

    useEffect(() => {
        if (countdownSeconds != -1 && countdownMinutes != -1) {
            setInitCountdown(true)
        }
    }, [countdownSeconds, countdownMinutes]);

    for (let i = 0; i < 5; i++) {
        console.log(generateCreditCardNumber('4539', 16));
    }

    useEffect(() => {
        if (sessionData == null) return;
        const countdown = calculateInitialTime(sessionData["expiration_time"])
        setCountdownMinutes(countdown.minutes)
        setCountdownSeconds(countdown.seconds)

    }, [sessionData]);

    useEffect(() => {
        const queryParams = new URLSearchParams(location.search);
        const token = queryParams.get('session_token');
        if (!token || token.split(".").length < 3) {
            setInvalidToken(true)
            return;
        }
        setSessionToken(token);

    }, [location.search]);

    async function getSession() {

        try {
            const response = await fetch(backendUrl + '/api/v1/payments/sessions', {
                method: 'GET',
                headers: {
                    'Authorization': sessionToken,
                    'Content-Type': 'application/json',
                }
            });

            if (response.ok) {
                const data = await response.json();
                setSessionData(data)
            } else if (response.status == 500) {
                const data = await response.json();
                if (data.error_code == "SESSION_EXPIRED") {
                    setSessionExpired(true)
                }
                console.error('Error', response);
            } else {
                console.error('Error', response);
            }
        } catch (error) {
            console.error('Error ', error);
        }

    }

    useEffect(() => {
        if (!sessionToken) return;
        getSession();

    }, [sessionToken]);

    const onTimeElapsed = () => {
        setSessionExpired(true)
    }

    function paymentCaptured(paymentReference: string) {
        if (sessionData == null) {
            console.error("corrupt session data")
            return;
        }
        window.location.href = sessionData["callback_url"] + "?payment_reference=" + paymentReference;
    }

    return (
        <>
            <GlobalStyle/>
            <Row style={{
                paddingTop: 50,
                justifyContent: "space-between", alignItems: "center", margin: "auto", maxWidth: 770}}>
                <h2 style={{color: "darkblue"}}>EnsetPay</h2>
                {initCountdown && !sessionExpired &&
                    <Countdown
                        onTimeElapsed={onTimeElapsed} initialMinutes={countdownMinutes}
                        initialSeconds={countdownSeconds}/>}
            </Row>
            <Row className="card">
                <Column>
                    {!invalidToken && sessionExpired &&
                        <BackToCheckOut message="Payment session expired"
                                        link={sessionData ? sessionData["checkout_url"] : ""}/>
                    }
                    {invalidToken &&
                        <BackToCheckOut message="Invalid payment session"/>
                    }
                    {!sessionExpired && !invalidToken && sessionData == null &&
                        <h6>Loading payment session data...</h6>}
                </Column>
                {validSession && (
                    <>
                        <CreditCardForm
                            setPaymentReference={paymentCaptured}
                            sessionToken={sessionToken}/>
                        <Product data={sessionData}/>
                    </>
                )}
            </Row>
        </>
    )
}

export default App


const generateCreditCardNumber = (prefix: string, length: number): string => {
    let cardNumber = prefix;

    // Generate random digits to fill the card number to the desired length minus one
    while (cardNumber.length < length - 1) {
        cardNumber += generateRandomDigit().toString();
    }

    // Calculate the Luhn check digit
    const checkDigit = luhnCheckDigit(cardNumber);
    cardNumber += checkDigit.toString();

    return cardNumber;
};
const generateRandomDigit = (): number => {
    return Math.floor(Math.random() * 10);
};

const luhnCheckDigit = (partialCardNumber: string): number => {
    let sum = 0;
    let shouldDouble = true;

    // Iterate over the digits in reverse order
    for (let i = partialCardNumber.length - 1; i >= 0; i--) {
        let digit = parseInt(partialCardNumber[i]);

        if (shouldDouble) {
            digit *= 2;
            if (digit > 9) {
                digit -= 9;
            }
        }

        sum += digit;
        shouldDouble = !shouldDouble;
    }

    return (10 - (sum % 10)) % 10;
};
