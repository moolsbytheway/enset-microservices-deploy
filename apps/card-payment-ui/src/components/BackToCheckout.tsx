import {Column, Link} from "./Components.tsx";


const BackToCheckOut = (props: { message: string, link?: string }) => {
    return <Column>
        <h6 style={{color: 'red'}}>{props.message}</h6>
        {props.link && <Link href={props.link}>Back to checkout</Link>}
    </Column>
}

export default BackToCheckOut;
