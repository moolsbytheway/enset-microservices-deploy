import {useEffect, useState} from 'react';
import styled from 'styled-components';
import {Row} from "./Components.tsx";

const TimerWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    color: indianred;

`;

const TimeDisplay = styled.div`
    display: flex;
    align-items: center;
`;

const Number = styled.span`
    margin: 0 5px;
`;

const Label = styled.span`
`;

export function calculateInitialTime(targetTime: string) {
    // Parse the target time string into a Date object
    const targetDate = new Date(targetTime);

    // Get the current time in milliseconds
    const currentDate = new Date();

    // Calculate the difference in milliseconds
    const differenceMs = targetDate.getTime() - currentDate.getTime();

    // Convert milliseconds to minutes and seconds
    const totalSeconds = Math.floor(differenceMs / 1000);
    const minutes = Math.floor(totalSeconds / 60);
    const seconds = totalSeconds % 60;

    // Return an object with minutes and seconds
    return {minutes, seconds};
}

const Countdown = (props: { initialMinutes: number, initialSeconds: number, onTimeElapsed: Function }) => {
    const [minutes, setMinutes] = useState(props.initialMinutes);
    const [seconds, setSeconds] = useState(props.initialSeconds);

    useEffect(() => {
        const countdown = setInterval(() => {
            if (seconds > 0) {
                setSeconds(seconds - 1);
            } else if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(countdown);
                    props.onTimeElapsed()
                } else {
                    setMinutes(minutes - 1);
                    setSeconds(59);
                }
            }
        }, 1000);

        return () => clearInterval(countdown);
    }, [minutes, seconds]);

    return (
        <Row style={{fontSize: 18}}>
            <h5>Session expires in:</h5>
            <TimerWrapper>
                <TimeDisplay>
                    <Number>{minutes < 10 ? `0${minutes}` : minutes}</Number>
                    <Label>:</Label>
                    <Number>{seconds < 10 ? `0${seconds}` : seconds}</Number>
                </TimeDisplay>
            </TimerWrapper>
        </Row>
    );
};

export default Countdown;
