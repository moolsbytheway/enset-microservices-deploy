// src/components/CreditCardForm.tsx
import {FormEvent, useEffect, useState} from 'react';
import {Button, Column, Form, Input, Row} from "./Components.tsx";
import {backendUrl} from "./Config.ts";

interface CreditCardFormProps {
    sessionToken: string
    setPaymentReference: Function
}

const CreditCardForm = (props: CreditCardFormProps) => {
    const [cardNumber, setCardNumber] = useState<string>('');
    const [name, setName] = useState<string>('');
    const [expiryDate, setExpiryDate] = useState<string>('');
    const [cvv, setCvv] = useState<string>('');
    const [cardSubmitted, setCardSubmitted] = useState(false)
    const [submitting, setSubmitting] = useState(false)
    const [formValid, setFormValid] = useState(false)
    const [ottPresent, setOttPresent] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")

    const [oneTimeToken, setOneTimeToken] = useState(null)

    useEffect(() => {
        const isValid =
            // check not null
            cardNumber != null && name != null && expiryDate != null && cvv != null &&
            // check length is correct
            cardNumber.length === 16 && name.length >= 2 && expiryDate.length === 5 &&
            (cvv.length === 3 || cvv.length === 4) &&
            // check numbers
            !isNaN(Number(cardNumber)) && validateCreditCard(cardNumber) && !isNaN(Number(cvv)) &&
            // check expiry date contains a slash
            expiryDate.split("/").length == 2 &&
            // check month and year are correctly formatted
            Number(expiryDate.split("/")[0]) <= 12 && Number(expiryDate.split("/")[0]) >= 1
            && Number(expiryDate.split("/")[1]) >= 24
        setFormValid(isValid)
    }, [cardNumber, name, expiryDate, cvv])


    useEffect(() => {
        setOttPresent(cardSubmitted && oneTimeToken != null)
    }, [cardSubmitted, oneTimeToken])


    const authorizeAndCapture = async () => {
        const requestBody = {
            type: "CREDIT_DEBIT_CARD",
            one_time_token: oneTimeToken
        };

        try {
            const response = await fetch(backendUrl + '/api/v1/payments/authorize-and-capture', {
                method: 'POST',
                headers: {
                    'Authorization': props.sessionToken,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody),
            });

            if (response.ok) {
                const data = await response.json();
                props.setPaymentReference(data.payment_reference);
            } else {
                console.error('Error ', response.statusText);
            }
        } catch (error) {
            console.error('Error ', error);
        }
    };

    const validateCreditCard = (cardNumber: string): boolean => {
        let sum = 0;
        let shouldDouble = false;

        // Iterate over the card number digits in reverse order
        for (let i = cardNumber.length - 1; i >= 0; i--) {
            let digit = parseInt(cardNumber[i]);

            if (shouldDouble) {
                digit *= 2;
                if (digit > 9) {
                    digit -= 9;
                }
            }

            sum += digit;
            shouldDouble = !shouldDouble;
        }

        return sum % 10 === 0;
    };

    function resetForm(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();
        setCardSubmitted(false)
        setSubmitting(false)
    }

    const vaultCard = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setSubmitting(true)
        // if card already vaulted go to authorizeAndCapture
        if (ottPresent) {
            await authorizeAndCapture();
            return;
        }
        const [expiryMonth, expiryYear] = expiryDate.split('/');

        const requestBody = {
            pan: cardNumber,
            expiry_year: `20${expiryYear}`,
            expiry_month: expiryMonth,
            cardholder_name: name,
            cvv: cvv,
        };

        try {
            const response = await fetch(backendUrl + '/api/v1/payments/add-credit-card', {
                method: 'POST',
                headers: {
                    'Authorization': props.sessionToken,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody),
            });

            if (response.ok) {
                const data = await response.json();
                console.log('Form submitted successfully:', data);
                setCardSubmitted(true);
                setOneTimeToken(data.one_time_token);
                setErrorMessage("")
            } else if (response.status == 400) {
                const data = await response.json();
                if (data.error_code == "CLIENT_ERROR") {
                    setErrorMessage(data.message)
                }
                setCardSubmitted(false);
                console.error('Error', response);
            } else {
                setErrorMessage("Server error, please contact your administrator")
                setCardSubmitted(false);
            }

            setSubmitting(false)
        } catch (error) {
            setSubmitting(false)
            setCardSubmitted(false);
            setErrorMessage("Unknown error occurred, please contact your administrator")
            console.error('Error', error);
        }
    };


    return (
        <>
            <Column>
                <h4 style={{
                    marginTop: 0,
                    flexWrap: "wrap",
                    maxWidth: 400
                }}>{cardSubmitted ? "Valid card added. Click 'Pay' to proceed" :
                    "Please fill in your card information to proceed"}</h4>
                {errorMessage && <small style={{color: "red"}}>{errorMessage}</small>}
                <Form onReset={resetForm} onSubmit={vaultCard}>
                    <Input
                        type="text"
                        placeholder="Card Number"
                        disabled={cardSubmitted || submitting}
                        value={cardNumber}
                        onChange={(e) => setCardNumber(e.target.value)}
                    />
                    <Input
                        type="text"
                        placeholder="Name on Card"
                        disabled={cardSubmitted || submitting}
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                    <Input
                        type="text"
                        placeholder="Expiry Date (MM/YY)"
                        disabled={cardSubmitted || submitting}
                        value={expiryDate}
                        onChange={(e) => setExpiryDate(e.target.value)}
                    />
                    <Input
                        type="password"
                        placeholder="CVV"
                        disabled={cardSubmitted || submitting}
                        value={cvv}
                        onChange={(e) => setCvv(e.target.value)}
                    />
                    <Row style={{justifyContent: "space-between"}}>
                        {cardSubmitted && !submitting &&
                            <Button style={{backgroundColor: "darkred", marginRight: 10}} type="reset">Change
                                card</Button>}
                        <Button
                            disabled={!formValid}
                            type="submit">
                            {submitting ? cardSubmitted ? "Processing payment..." : "Adding card..." :
                                cardSubmitted ? "Pay" : "Add card"}</Button></Row>
                </Form>
            </Column>
        </>
    )
        ;
};

export default CreditCardForm;
