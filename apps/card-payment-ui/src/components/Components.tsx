import styled from 'styled-components';

const Row = styled.div`
    display: flex;
    flex-direction: row;
    
`;
const Center = styled.div`
    display: flex;
    justify-content: center;
`;

const Column = styled.div`
    display: flex;
    flex-direction: column;
`;
const Form = styled.form`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin: 0 30px 30px auto;
    @media all and (min-width: 700px) {
        width: 400px;
    }
`;

const Input = styled.input`
    margin-bottom: 10px;
    padding: 8px;
    font-size: 14px;
    border: 1px solid #ccc;
    border-radius: 4px;
`;

const Button = styled.button`
    padding: 10px;
    background-color: darkblue;
    color: white;
    width: 100%;
    border: none;
    border-radius: 4px;
    font-size: 16px;
    cursor: pointer;

    &:hover {
        background-color: darkblue;
    }

    &:disabled {
        background-color: #aaa;
    }
`;
const Link = styled.a`
    padding: 10px;
    background-color: darkblue;
    color: white;
    width: 100%;
    border: none;
    border-radius: 4px;
    font-size: 16px;
    cursor: pointer;

    &:hover {
        background-color: darkblue;
    }

    &:disabled {
        background-color: #aaa;
    }
`;

const Image = styled.img`
    width: 100%;
    height: auto;
    border-radius: 8px;
`;

const Details = styled.div`
    margin-top: 16px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
`;

const DetailItem = styled.div`
    margin-bottom: 8px;
    margin-right: 20px;
    font-size: 14px;
`;


export {Link, Center, Column, Form, Button, Input, Row, Image, Details, DetailItem}
