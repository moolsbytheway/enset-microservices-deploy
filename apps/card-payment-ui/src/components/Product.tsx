// src/components/CreditCardForm.tsx
import {FC} from 'react';
import {Column, DetailItem, Details} from "./Components.tsx";


interface ProductFormProps {
    data: any
}

const Product: FC<ProductFormProps> = (props: ProductFormProps) => {
    return (
        <Column style={{marginTop: 0}}>
            <Details style={{marginTop: 0}}>
                <DetailItem><h3 style={{color: "blueviolet", margin: 0}}>{props.data.order.description}</h3>
                </DetailItem>
                <DetailItem style={{color: "red", fontSize: 20}}><strong>Total
                    amount:</strong> {(props.data.order.amount * props.data.order.quantity).toFixed(2)} DH</DetailItem>
                <DetailItem><strong>Unit price:</strong> {props.data.order.amount.toFixed(2)} DH</DetailItem>
                <DetailItem><strong>Quantity:</strong> {props.data.order.quantity}</DetailItem>

                <DetailItem><strong>Order ID:</strong> {props.data.order.order_id}</DetailItem>
                <DetailItem><strong>Name:</strong> {props.data.customer.name}</DetailItem>
                <DetailItem><strong>Email:</strong> {props.data.customer.email}</DetailItem>
                <DetailItem><strong>Address:</strong> {props.data.customer.address}</DetailItem>
                <DetailItem><strong>Phone:</strong> {props.data.customer.phone}</DetailItem>
            </Details>
        </Column>
    );
};

export default Product;
