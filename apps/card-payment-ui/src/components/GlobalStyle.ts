import {createGlobalStyle} from 'styled-components';

const GlobalStyle = createGlobalStyle`
    body {
        font-family: Inter, sans-serif;
        background: linear-gradient(135deg, #f5f5f8 22px, rgba(0, 0, 0, 0.04) 0, rgba(0, 0, 0, 0.04) 24px, transparent 0, transparent 67px, rgba(0, 0, 0, 0.04) 0, rgba(0, 0, 0, 0.04) 69px, transparent 0), linear-gradient(225deg, #f5f5f8 22px, rgba(0, 0, 0, 0.04) 0, rgba(0, 0, 0, 0.04) 24px, transparent 0, transparent 67px, rgba(0, 0, 0, 0.04) 0, rgba(0, 0, 0, 0.04) 69px, transparent 0) 0 64px;
        background-color: #f5f5f8;
        background-size: 64px 128px;

        height: 100vh;

    }

    .root {
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 0;
        padding: 0;
    }

    .card {
        display: flex;
        text-align: center;
        background: white;
        max-width: 700px;
        margin: auto;
        padding: 40px;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);

        @media all and (max-width: 700px) {
            flex-wrap: wrap;
        }
    }

    h1, h2, h3, h4, h5, h6 {
        margin-bottom: 10px;
        margin-top: 10px;
    }
`;

export default GlobalStyle;
