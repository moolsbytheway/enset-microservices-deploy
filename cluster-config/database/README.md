# mariadb

- helm upgrade mariadb bitnami/mariadb --install --namespace dev --create-namespace -f ./mariadb-dev.yaml

# redis

- helm upgrade redis bitnami/redis --install --namespace dev --create-namespace -f ./redis-dev.yaml
